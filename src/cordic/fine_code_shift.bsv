package fine_code_shift;

    import cordicpipe_main::*;
    import magnitudepipe::*;
    import BRAMCore::*;
    import Vector::*;
    `include "defined_parameters.bsv"
    
    interface Ifc_fine_code_shift#(numeric type samples,
                                   numeric type input_length,
                                   numeric type frequency_component_length,
                                   numeric type max_spcc,
                                   numeric type max_prn,
                                   numeric type min_hspcc,
                                   numeric type odd_spcc,
                                   numeric type even_spcc,
                                   numeric type magnitude_length);
        method Action get_inputs(Bit#(10) prompt_prn, Bit#(TLog#(max_spcc)) stop_sample, Bit#(TLog#(max_prn)) satellite_no);
        method Bit#(TLog#(max_spcc)) result_corrected_prn_sample;
    endinterface : Ifc_fine_code_shift

    module mkfine_code_shift(BRAM_PORT#(Bit#(TAdd#(TLog#(max_prn),10)),Bit#(1)) prn_val,
                             BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                             Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_component_length,cordic_additional_scalling,ms_of_data_processed) cordic_main,
                             Ifc_magnitudepipe#(magnitude_cordic_iterations,magnitude_scalling_in_processing,magnitude_length) magnitudepipe,
                             Ifc_fine_code_shift#(samples,input_length,frequency_component_length,max_spcc,max_prn,min_hspcc,odd_spcc,even_spcc,magnitude_length) fine_codeshift
                            )
                            provisos(Add#(a,TAdd#(input_length,frequency_component_length),magnitude_length));

        Bit#(TLog#(max_spcc)) count = fromInteger(valueOf(TMul#(min_hspcc,2))+1);
        //Vector#(TAdd#(TMul#(min_hspcc,2),1),Reg#(Bit#(TLog#(max_spcc)))) current_sample <- replicateM(mkReg(0));
        //Vector#(TAdd#(TMul#(min_hspcc,2),1),Reg#(Bit#(TLog#(max_spcc)))) sample_reference <- replicateM(mkReg(0));
        Reg#(Bit#(TLog#(max_spcc))) rg_current_sample <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_sample_reference <- mkReg(0);
        Reg#(Bit#(TAdd#(10,TLog#(max_prn)))) rg_prn_address <- mkReg(0);
        Reg#(Bit#(TLog#(samples))) rg_accumulate_count <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_magnitude_compare <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_corrected_sample <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_count <- mkReg(0);
        Vector#(TAdd#(TMul#(min_hspcc,2),1),Reg#(Bit#(1))) prn_value <- replicateM(mkReg(0));
        Vector#(TAdd#(TMul#(min_hspcc,2),1),Reg#(Int#(magnitude_length))) inphase <- replicateM(mkReg(0));
        Vector#(TAdd#(TMul#(min_hspcc,2),1),Reg#(Int#(magnitude_length))) quadphase <- replicateM(mkReg(0));

        Reg#(Bit#(3)) rg_flag <- mkReg(0);

        for(Bit#(TLog#(max_spcc)) i = fromInteger(valueOf(min_hspcc)); i < count; i = i+1)
        begin
            rule r1_current_prn_update_upper(rg_flag == 1);
                prn_value[i] <= prn_val.read;
                //$display($time," %d : prn update lower %d \n",i,prn_val.read);
            endrule
        end
        rule r1_prn_and_sample_fetch_lower(rg_flag == 1);
            if(rg_current_sample == fromInteger(valueOf(min_hspcc)))
                prn_val.put(False,rg_prn_address-1,?);
            //$display($time," prn upper fetch \n");
            rg_flag <= 2;
        endrule

        for(Bit#(TLog#(max_spcc)) i = 0; i<=fromInteger(valueOf(min_hspcc)-1); i=i+1)
        begin
            rule r2_current_prn_update_lower(rg_flag == 2);
                prn_value[i] <= prn_val.read;
                //$display($time," %d : prn update upper %d \n",i,prn_val.read);
            endrule
        end
        rule r2_initial_prn_fetch(rg_flag == 2);
            Bit#(1) temp_prn_ref = prn_val_sample.read;
            if(temp_prn_ref == 1)
                rg_sample_reference <= fromInteger(valueOf(odd_spcc)-1);
            else
                rg_sample_reference <= fromInteger(valueOf(even_spcc)-1);
            if(rg_prn_address[9:0] == 1022)
            begin
                Bit#(TAdd#(TLog#(max_prn),10)) temp_prn_address = rg_prn_address - 1022;
                rg_prn_address <= temp_prn_address;
                prn_val.put(False,temp_prn_address,?);
                prn_val_sample.put(False,0,?);
            end
            else
            begin
                Bit#(TAdd#(TLog#(max_prn),10)) temp_prn_address = rg_prn_address + 1;
                rg_prn_address <= temp_prn_address;
                prn_val.put(False,temp_prn_address,?);
                prn_val_sample.put(False,temp_prn_address[9:0],?);
            end
            //$display($time,"%d : reference sample : %d \n",rg_accumulate_count,temp_prn_ref);
            rg_accumulate_count <= 1;
            rg_flag <= 3;
        endrule

        for(Bit#(TLog#(max_spcc)) i = 0; i<count; i=i+1)
        begin
            rule r3_correlation(rg_flag == 3);
                let in_p = cordic_main.i_out;
                let quad_p = cordic_main.q_out;
                if(prn_value[i] == 1)
                begin
                    inphase[i] <= inphase[i] + signExtend(in_p);
                    quadphase[i] <= quadphase[i] + signExtend(quad_p);
                end
                else
                begin
                    inphase[i] <= inphase[i] - signExtend(in_p);
                    quadphase[i] <= quadphase[i] - signExtend(quad_p);
                end
                //$display($time," %d : accumulated_i = %d and accumulated_q = %d \n", i, inphase[i],quadphase[i]);
                if(i!=count-1)
                    prn_value[i] <= prn_value[i+1];
            endrule
        end
        rule r3_prn_and_count_update(rg_flag == 3);
            let i = cordic_main.i_out;
            let q = cordic_main.q_out;
            $display($time," %d : obtained inphase = %d quadphase = %d \n",rg_accumulate_count,i,q);
            rg_accumulate_count <= rg_accumulate_count + 1;
            if(rg_current_sample == rg_sample_reference)
            begin
                rg_current_sample <= 0;
                    
                Bit#(1) temp_prn_ref = prn_val_sample.read;
                if(temp_prn_ref == 1)
                    rg_sample_reference <= fromInteger(valueOf(odd_spcc)-1);
                else
                    rg_sample_reference <= fromInteger(valueOf(even_spcc)-1);
                prn_value[count-1] <= prn_val.read;

                if(rg_prn_address[9:0] == 1022)
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_prn_address = rg_prn_address - 1022;
                    rg_prn_address <= temp_prn_address;
                    prn_val.put(False,temp_prn_address,?);
                    prn_val_sample.put(False,0,?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_prn_address = rg_prn_address + 1;
                    rg_prn_address <= temp_prn_address;
                    prn_val.put(False,temp_prn_address,?);
                    prn_val_sample.put(False,temp_prn_address[9:0],?);
                end
            end
            else
                rg_current_sample <= rg_current_sample + 1;
            if(rg_accumulate_count == fromInteger(valueOf(samples)-1))
                rg_flag <= 4;
        endrule
        rule r4_magnitude_compute(rg_flag == 4);
            magnitudepipe.get_inputs(inphase[rg_count],quadphase[rg_count]);
            $display($time,"%d : Inputs to magnitude pipe %d and %d \n",rg_count,inphase[rg_count],quadphase[rg_count]);
            rg_flag <= 5;
        endrule
        rule r5_magnitude_compare(rg_flag == 5);
            Int#(magnitude_length) temp_mag <- magnitudepipe.result_magnitude;
            $display($time,"%d : magnitude = %d \n ",rg_count,temp_mag);
            if(temp_mag > rg_magnitude_compare)
            begin
                rg_magnitude_compare <= temp_mag;
                rg_corrected_sample <= rg_count;
            end
            if(rg_count < count - 1)
            begin
                rg_count <= rg_count + 1;
                rg_flag <= 4;
            end
            else
                rg_flag <= 6;
        endrule

        method Action get_inputs(Bit#(10) prompt_prn, Bit#(TLog#(max_spcc)) stop_sample, Bit#(TLog#(max_prn)) satellite_no);
            Bit#(TAdd#(10,TLog#(max_prn))) temp_address = zeroExtend(satellite_no)*1024;
            rg_prn_address <= temp_address + zeroExtend(prompt_prn);
            prn_val.put(False,temp_address + zeroExtend(prompt_prn),?);
            prn_val_sample.put(False,prompt_prn,?);
            rg_current_sample <= stop_sample;
            rg_magnitude_compare <= 0;
            rg_flag <= 1;
            $display($time,"Start prn = %d, start sample = %d and satellite number %d \n",prompt_prn, stop_sample, satellite_no);
        endmethod
        method Bit#(TLog#(max_spcc)) result_corrected_prn_sample if(rg_flag == 6);
            return rg_corrected_sample;
        endmethod
        
    endmodule : mkfine_code_shift
    
endpackage : fine_code_shift