import Vector ::*;
import Real::*;
import FixedPoint::*;
`include "defined_parameters.bsv"
//import defined_types::*;
import freqsynthesiser ::*;

module mkTestbench();

	//Reg#(Int#(16)) interval <- mkReg(500);
	//Reg#(Int#(32)) samp_freq <- mkReg(12000000);//12 MHz
	//Reg#(Int#(24)) base_freq <- mkReg(10000); //-10 KHz
	Reg#(Bit#(1)) flag <- mkReg(0);
	
	Ifc_freqsynthesiser#(TAdd#(TLog#(`Doppler_interval),1),TAdd#(TLog#(`Start_frequency),1),TAdd#(`Angle_length,1),TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) ifc_tb <- mkfreqsynthesiser;
	//Ifc_freqsynthesiser#(10,23,33,10,20) ifc_tb <- mkfreqsynthesiser;
	rule r1_give_inputs(flag == 0);
		ifc_tb.get_inputs(`Doppler_interval, 357.9139413333,`Start_frequency);
		flag <= 1;
	endrule
	
	rule r2_get_results(flag == 1);
		let a = ifc_tb.results_base();
		let b = ifc_tb.results_interval();
		$display("\n base angle = %d",a);
		$display("\n Interval angle = %d",b);
		$finish;
	endrule
endmodule : mkTestbench
