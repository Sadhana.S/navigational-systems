/*
Module: Tan Inverse Computation
Description: This module computes the Tan Inverse of an input using Cordic.
			 The module performes 31 iterations of the cordic algorithm so as to obtain the value of
			 Tan inverse. The number of iterations can be reduced depending upon the accuracy
			 needed.
*/
package arctan;

    import Vector ::*;

    interface Ifc_arctan#(numeric type angle_length,
                          numeric type magnitude_length,
                          numeric type cordic_additional_length_arctan,
                          numeric type cordic_iterations_arctan
                          );
        method Action get_inputs(Int#(magnitude_length) i_in,Int#(magnitude_length) q_in);
        method Int#(angle_length) result_phase();
        method Int#(magnitude_length) result_magnitude();
    endinterface : Ifc_arctan

    module mkarctan(Ifc_arctan#(angle_length,magnitude_length,cordic_additional_length_arctan,cordic_iterations_arctan))
                    provisos(Add#(magnitude_length,cordic_additional_length_arctan,a)
                            );

		//This vector stores the values of arctan(2^0) i.e. 45degrees, arctan(2^-1)
		//i.e. 26.565degrees, arctan(2^-2) i.e. 14.036degrees, etc. for upto 31 values.
        Vector#(31, Int#(angle_length)) atan_table;
        atan_table[0] = 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
        atan_table[1] = 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
        atan_table[2] = 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
        atan_table[3] = 'b00000101000100010001000111010100; // atan(2^-3)
        atan_table[4] = 'b00000010100010110000110101000011;
        atan_table[5] = 'b00000001010001011101011111100001;
        atan_table[6] = 'b00000000101000101111011000011110;
        atan_table[7] = 'b00000000010100010111110001010101;
        atan_table[8] = 'b00000000001010001011111001010011;
        atan_table[9] = 'b00000000000101000101111100101110;
        atan_table[10]= 'b00000000000010100010111110011000;
        atan_table[11]= 'b00000000000001010001011111001100;
        atan_table[12]= 'b00000000000000101000101111100110;
        atan_table[13]= 'b00000000000000010100010111110011;
        atan_table[14]= 'b00000000000000001010001011111001;
        atan_table[15]= 'b00000000000000000101000101111100;
        atan_table[16]= 'b00000000000000000010100010111110;
        atan_table[17]= 'b00000000000000000001010001011111;
        atan_table[18]= 'b00000000000000000000101000101111;
        atan_table[19]= 'b00000000000000000000010100010111;
        atan_table[20]= 'b00000000000000000000001010001011;
        atan_table[21]= 'b00000000000000000000000101000101;
        atan_table[22]= 'b00000000000000000000000010100010;
        atan_table[23]= 'b00000000000000000000000001010001;
        atan_table[24]= 'b00000000000000000000000000101000;
        atan_table[25]= 'b00000000000000000000000000010100;
        atan_table[26]= 'b00000000000000000000000000001010;
        atan_table[27]= 'b00000000000000000000000000000101;
        atan_table[28]= 'b00000000000000000000000000000010;
        atan_table[29]= 'b00000000000000000000000000000001;
        atan_table[30]= 'b00000000000000000000000000000000;

        Reg#(Int#(TAdd#(magnitude_length,cordic_additional_length_arctan))) rg_magnitude <- mkReg(0);
        Reg#(Int#(TAdd#(magnitude_length,cordic_additional_length_arctan))) rg_temp <- mkReg(0);
        Reg#(Int#(angle_length)) rg_phase <- mkReg(0);

        Reg#(Bit#(TAdd#(TLog#(cordic_iterations_arctan),1))) rg_iterations_count <- mkReg(fromInteger(valueOf(cordic_iterations_arctan)));

		//This is the core rule of the cordic algorithm which is performed iteratively for 31 clock
		//cycles.
        rule cordic_iterations(rg_iterations_count<fromInteger(valueOf(cordic_iterations_arctan)));
            if(rg_temp < 0)
            begin
                rg_magnitude <= rg_magnitude - (rg_temp >> rg_iterations_count);
                rg_temp <= rg_temp + (rg_magnitude >> rg_iterations_count);
                rg_phase <= rg_phase - atan_table[rg_iterations_count];
            end
            else
            begin
                rg_magnitude <= rg_magnitude + (rg_temp >> rg_iterations_count);
                rg_temp <= rg_temp - (rg_magnitude >> rg_iterations_count);
                rg_phase <= rg_phase + atan_table[rg_iterations_count];
            end
			rg_iterations_count <= rg_iterations_count + 1;
        endrule

        method Action get_inputs(Int#(magnitude_length) i_in,Int#(magnitude_length) q_in);
			let i= i_in;
			let q= q_in;
			if(i_in < 0) begin
            	i = -i_in;
            	q = -q_in;
			end
			rg_magnitude<= zeroExtend(i);
            rg_temp<= signExtend(q);	
            rg_phase <= 0;
            rg_iterations_count <= 0;
        endmethod
        method Int#(angle_length) result_phase if(rg_iterations_count==fromInteger(valueOf(cordic_iterations_arctan)));
            return rg_phase;
        endmethod
        method Int#(magnitude_length) result_magnitude if(rg_iterations_count==fromInteger(valueOf(cordic_iterations_arctan)));
            return truncate(rg_magnitude);
        endmethod
    endmodule : mkarctan

endpackage : arctan
