import cordicpipe_main::*;
import cordicpipe_finefrequencyshift::*;
import BRAMCore::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Integer samp1 = valueOf(`Samples);
    Integer prn = valueOf(`Max_PRN)*1024;
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_val_12",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_12",False);  

    Reg#(Bit#(3)) rg_flag <- mkReg(0);
    Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
    Reg#(Bit#(TLog#(`Samples))) rg_count <- mkReg(0);
    Reg#(Bit#(TLog#(`Samples_correlated))) rg_bram_count <- mkReg(0);

    Ifc_cordicpipe_main#(`Angle_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Cordic_additional_scalling,`Data_processed) cordicpipe <- mkcordicpipe_main(i_val,q_val);
    Ifc_cordicpipe_finefrequencyshift#(`Samples,`Input_length,`Frequency_component_length,`Magnitude_length,`Accumulated_length,`Max_PRN,`Max_spcc,`Odd_spcc,`Even_spcc) finefreq <- mkcordicpipe_finefrequencyshift(prn_val,prn_val_sample,cordicpipe);

    rule r1(rg_flag == 0);
        cordicpipe.initialise(1271668230,178956,0,0);
        finefreq.initialise(1022,0,0);
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        let i = finefreq.inphase_out;
        let q = finefreq.quadphase_out;
        $display($time," i = %d and q = %d \n",i,q);
        $finish;
    endrule
endmodule