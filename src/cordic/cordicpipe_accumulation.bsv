package cordicpipe_accumulation;

import BRAM::*;
import BRAMCore::*;
import GetPut::*;
import ClientServer::*;
import cordicpipe_main::*;

interface Ifc_cordicpipe_accumulation#(numeric type input_length,
                                       numeric type frequency_component_length,
                                       numeric type samples_correlated,
                                       numeric type accumulated_length,
                                       numeric type max_spcc,
                                       numeric type common_hspcc,
                                       numeric type different_hspcc
                                       );
    method Action initialise;
    method Action bram_address(Bit#(TLog#(samples_correlated)) address_in);
    method ActionValue#(Int#(accumulated_length)) accumulated_i_out;
    method ActionValue#(Int#(accumulated_length)) accumulated_q_out;
endinterface : Ifc_cordicpipe_accumulation

function BRAMRequest#(Bit#(a), Int#(accumulated_length)) makeRequest(Bool write, Bit#(a) addr, Int#(accumulated_length) data);
    return BRAMRequest{
    write: write,
    responseOnWrite:False,
    address: addr,
    datain: data
    };
endfunction

module mkcordicpipe_accumulation(BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                                 Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_component_length,cordic_additional_scalling,ms_of_data_processed) ifc_cordic_main,
                                 Ifc_cordicpipe_accumulation#(input_length,frequency_component_length,samples_correlated,accumulated_length,
                                                              max_spcc,common_hspcc,different_hspcc) ifc_cordic_acc)
                                 provisos(Log#(samples_correlated,a),
                                          Add#(TLog#(max_spcc),TAdd#(input_length,frequency_component_length),accumulated_length)
                                         );

    Bit#(TLog#(max_spcc)) hspcc_C = fromInteger(valueOf(common_hspcc));
    Bit#(TLog#(max_spcc)) hspcc_D = fromInteger(valueOf(different_hspcc));  

    BRAM_Configure cfg1 = defaultValue;
    BRAM1Port#(Bit#(TLog#(samples_correlated)),Int#(accumulated_length)) inphase <- mkBRAM1Server(cfg1);
    BRAM1Port#(Bit#(TLog#(samples_correlated)),Int#(accumulated_length)) quadphase <- mkBRAM1Server(cfg1);
    Reg#(Bit#(TLog#(samples_correlated))) rg_bram_count <- mkReg(fromInteger(valueOf(samples_correlated)));

    Reg#(Int#(accumulated_length)) rg_accumulate_I <- mkReg(0);
    Reg#(Int#(accumulated_length)) rg_accumulate_Q <- mkReg(0);
    Reg#(Bit#(TLog#(max_spcc))) rg_current_count <- mkReg(0);
    Reg#(Bit#(TLog#(max_spcc))) rg_count_reference <- mkReg(fromInteger(valueOf(common_hspcc)));

    rule accumulate_and_push_to_bram(rg_bram_count < fromInteger(valueOf(samples_correlated)));
        let i = ifc_cordic_main.i_out;
        let q = ifc_cordic_main.q_out;
        if(rg_current_count == rg_count_reference)
        begin
            inphase.portA.request.put(makeRequest(True,rg_bram_count,rg_accumulate_I));
            quadphase.portA.request.put(makeRequest(True,rg_bram_count,rg_accumulate_Q));
            rg_accumulate_I <= signExtend(i);
            rg_accumulate_Q <= signExtend(q);
            if(rg_bram_count[0] == 1)
            begin
                Bit#(10) temp = rg_bram_count[10:1] + 1;
                if(temp == 1023)
                    prn_val_sample.put(False,0,?);
                else
                    prn_val_sample.put(False,temp,?);
                rg_count_reference <= hspcc_C;
            end
            else
            begin
                Bit#(1) temp = prn_val_sample.read;
                if(temp == 1)
                    rg_count_reference <= hspcc_D;
                else
                    rg_count_reference <= hspcc_C;
            end
        rg_current_count <= 1;
        rg_bram_count <= rg_bram_count + 1;
        end
        else
        begin
            rg_accumulate_I <= rg_accumulate_I + signExtend(i);
            rg_accumulate_Q <= rg_accumulate_Q + signExtend(q);
            rg_current_count <= rg_current_count + 1;
        end
        //$display($time," %d : count = %d and ref = %d : i = %d & q = %d \n",rg_bram_count,rg_current_count,rg_count_reference,rg_accumulate_I,rg_accumulate_Q);
    endrule

    method Action initialise;
        prn_val_sample.put(False,0,?);
        rg_current_count <= 0;
        rg_bram_count <= 0;
        rg_count_reference <= fromInteger(valueOf(common_hspcc));
        rg_accumulate_I <= 0;
        rg_accumulate_Q <= 0;
    endmethod
    method Action bram_address(Bit#(TLog#(samples_correlated)) address_in) if(rg_bram_count == fromInteger(valueOf(samples_correlated)));
        inphase.portA.request.put(makeRequest(False,address_in,?));
        quadphase.portA.request.put(makeRequest(False,address_in,?));
    endmethod
    method ActionValue#(Int#(accumulated_length)) accumulated_i_out;
        let cosine_val <- inphase.portA.response.get;
        return cosine_val;
    endmethod
    method ActionValue#(Int#(accumulated_length)) accumulated_q_out;
        let sine_val <- quadphase.portA.response.get;
        return sine_val;  
    endmethod
endmodule : mkcordicpipe_accumulation
    
endpackage : cordicpipe_accumulation