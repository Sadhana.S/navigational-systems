import phasefilter::*;
`include "defined_parameters.bsv"

module mkTestbench();

Ifc_phasefilter#(`Angle_length) phasefilter <- mkphasefilter(1,0);

Reg#(Bit#(1)) flag <- mkReg(0);
Reg#(Int#(`Angle_length)) phase1 <- mkReg(161061273);
Reg#(Int#(`Angle_length)) phase2 <- mkReg(92341796);
Reg#(Int#(`Angle_length)) phase_out <- mkReg(0);

rule r1(flag==0);
phasefilter.get_inputs(phase1,phase2);
flag <= 1;
endrule
rule r2(flag==1);
let a = phasefilter.results();
phase_out <= a;
$display("\n Input phases = %d and %d  Output of phase filter = %d",phase1,phase2,a);
$finish;
endrule

endmodule
