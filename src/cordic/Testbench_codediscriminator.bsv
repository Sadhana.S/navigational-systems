import codediscriminator::*;
import FixedPoint::*;

module mkTestbench();

Ifc_codediscriminator codediscriminator <- mkcodediscriminator;

Reg#(Int#(3)) flag1 <- mkReg(0);
Reg#(Int#(32)) early_in <- mkReg(16163245);
Reg#(Int#(32)) late_in <- mkReg(9832549);
Reg#(Int#(5)) fact_in <- mkReg(5);
Reg#(Int#(5)) out_temp <- mkReg(0);

rule rl1(flag1==0);
codediscriminator.get_inputs(early_in,late_in,fact_in);
flag1<=1;
endrule

rule rl2(flag1==1);
out_temp <= codediscriminator.result();
flag1<=2;
endrule

rule rl3(flag1==2);
$display("\n Early_in = %d, Late_in = %d, Factor = %d, Code Error = %d",early_in,late_in,fact_in,out_temp);
$finish(0);
endrule
endmodule
