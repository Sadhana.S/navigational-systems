/*
Module: Discriminator Function
Description: This module computes (E-L)/(E+L) where E is the early code and L is the late code shifted version of the freq shifted input signal. It also rounds off this value to the nearest integer value.  
*/

package codediscriminator;

    import FixedPoint::*;

    interface Ifc_codediscriminator;
        method Action get_inputs(Int#(32) e_in, Int#(32) l_in, Int#(5) factor_in);
        method Int#(5) result();
    endinterface : Ifc_codediscriminator

    module mkcodediscriminator(Ifc_codediscriminator);

        Reg#(Int#(32)) rg_N <- mkReg(0);
        Reg#(Int#(32)) rg_D <- mkReg(0);
        Reg#(Int#(5)) rg_factor <- mkReg(0);
        Reg#(FixedPoint#(5,7)) rg_intr <- mkReg(0);
        Reg#(Int#(5)) rg_out <- mkReg(0);

        Reg#(Bit#(1)) readFlag <- mkReg(0);
        Reg#(Bit#(2)) flag <- mkReg(0);

        rule r0_divider((readFlag==1)&&(flag==0));
            FixedPoint#(32,16) temp1_1 = fromInt(rg_N);//(E-L)
            FixedPoint#(15,16) temp1 = fxptTruncate(temp1_1);//(E-L)
            FixedPoint#(32,16) temp2_2 = fromInt(rg_D);//(E+L)
            FixedPoint#(15,16) temp2 = fxptTruncate(temp2_2);//(E+L)
            FixedPoint#(32,16) temp3 = fxptQuot(temp1,temp2);
            FixedPoint#(2,6) temp4 = fxptTruncate(temp3);//((E-L)/(E+L))
            //temp1 and temp2 need to be truncated before multiplication otherwise temp3 range would become (49,16) which exceeds the 64-bit range of fixedpoint and throw a link error. 

            FixedPoint#(5,1) temp5 = fromInt(rg_factor);
            rg_intr <= fxptTruncate(fxptMult(temp4,temp5));
            flag <= 1;
        endrule
        rule rl_roundingoff(flag==1);//General Round off function
            UInt#(7) temp5 = fxptGetFrac(rg_intr);
            if(temp5 < 64)  //TODO check only first fractional bit.
							//try if temp5[5]!=1 works. Also, what if this is removed?
                rg_out <= fxptGetInt(rg_intr) - 1;//If fractional part is lesser than 0.5
            else
                rg_out <= fxptGetInt(rg_intr) + 1;//If fractional part is greater than 0.5
	flag<=2;
        endrule

        method Action get_inputs(Int#(32) e_in, Int#(32) l_in, Int#(5) factor_in) if(readFlag == 0);
            rg_N <= e_in - l_in;

            rg_D <= e_in + l_in;
            rg_factor <= factor_in;//(Samples per code chip/2)
            readFlag <= 1;
        endmethod
        method Int#(5) result() if(flag==2);
		//flag<= 0;
		//readFlag<= 0;
            return rg_out;
        endmethod
    endmodule : mkcodediscriminator

endpackage : codediscriminator
