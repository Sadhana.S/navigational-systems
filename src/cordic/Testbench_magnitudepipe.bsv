import magnitudepipe::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Reg#(Bit#(2)) rg_count <- mkReg(0);

    Ifc_magnitudepipe#(`Magnitude_cordic_iterations,`Magnitude_cordic_pipe_length,`Magnitude_scalling_in_processing,`Magnitude_length) magnitudepipe <- mkmagnitudepipe;

    rule r1(rg_count == 0);
        magnitudepipe.get_inputs(136667,17926);
        rg_count <= 1;
    endrule
    rule r2(rg_count == 1);
        let a = magnitudepipe.result_magnitude();
        $display($time,"Output : %d \n",a);
        $finish;
    endrule
    
endmodule : mkTestbench