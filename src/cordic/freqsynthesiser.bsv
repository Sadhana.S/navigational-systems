//This package gives the base angle and the interval angle which has to be added every time in the angle accumulator
//Input : Scalled Sampling frequency (2^32/sampling frequency), Doppler frequency shift interval and base frequency(the base doppler frequency such as Intermediate Frequency(IF)-10 KHz or -10KHz for acquisition)
//Output : Base angle in scalled version of Base frequency and the interval angle in scalled version of Doppler frequency interval

//1. The scaling factor is left shifted by 31 to make it an Integer (from Fixed Point) so as to avoid fixed point computations thereafter.
//2. We do not actually compute (2*pi*fd/fs). Angle lookup happens in degrees, and not radians. Therefore, we need to compute (2*pi*fd/fs)*(180/pi)= (2*fd*180/fs). Also,
//   since there is a scaling factor, where 360 degrees corresponds to 2^32 => 180 degrees corresponds to 2^31, we need to compute (2*fd*180/fs)*(2^31)/180 = (2*fd/fs)*(2^31)= (fd/fs)<<32
//3. 2^32/fs is given as scalled sampling frequency to avoid division and increase the precision of angle value generated 
//4. TODO Add conditions in provisos to make 'y' and 'z' non-zero
//5. What happens when the angle length is decreased lesser than the length of samping frequency?

package freqsynthesiser;

	import Real::*;
	import FixedPoint::*;
	import Vector::*;
	import BRAMCore::*;
	
	interface Ifc_freqsynthesiser#(numeric type size_i,//Size of Doppler frequency interval
								   numeric type size_s,//Size of Start frequency(IF-Doppler_frequency)
								   numeric type size_a,//Size of angle given to Cordic
								   numeric type size_sf_i,//Integer length of Scalled inverse sampling frequency
								   numeric type size_sf_f);//Fractional length of Scalled inverse sampling frequency

		method Action get_inputs(Int#(size_i) interval_in, FixedPoint#(size_sf_i,size_sf_f) scalled_samp_freq_in,Int#(size_s) base_freq_in);
		method Int#(size_a) results_base();		//Scaled version of base freq. Scaled by ((2*pi/fs)<<31)
		method Int#(size_a) results_interval();	//Scaled version of the interval_in
	
	endinterface : Ifc_freqsynthesiser
	
	module mkfreqsynthesiser(Ifc_freqsynthesiser#(size_i,size_s,size_a,size_sf_i,size_sf_f))
							provisos(Add#(size_sf_i,x,size_a),
									 Add#(y,size_i,x),
									 Add#(z,size_s,x));

		Reg#(Bit#(1)) flag <- mkReg(0);
		Reg#(Int#(size_i)) rg_interval <- mkReg(0);
		Reg#(Int#(size_s)) rg_base_freq <- mkReg(0);
		Reg#(FixedPoint#(size_sf_i,size_sf_f)) rg_scalled_samp_freq <- mkReg(0);
		Reg#(Int#(size_a)) rg_interval_angle <- mkReg(0);
		Reg#(Int#(size_a)) rg_base_angle <- mkReg(0);

		rule r1_freq_calc(flag == 1);//computation of base_freq*scalled sampling frequency and interval_freq*scalled sampling frequency
			Int#(x) temp_1 = zeroExtend(rg_base_freq);
			Int#(x) temp_2 = zeroExtend(rg_interval);
			FixedPoint#(x,1) temp1 = fromInt(temp_1);
			FixedPoint#(x,1) temp2 = fromInt(temp_2);
			Int#(size_a) temp3 = fxptGetInt(fxptMult(rg_scalled_samp_freq,temp1));
			Int#(size_a) temp4 = fxptGetInt(fxptMult(rg_scalled_samp_freq,temp2));
			//$display($time,"Base angle : %d \n",temp3);
			//$display($time,"Interval angle : %d\n",temp4);
			rg_base_angle <= temp3;
			rg_interval_angle <= temp4;
			flag <= 0;
		endrule
		
		method Action get_inputs(Int#(size_i) interval_in, FixedPoint#(size_sf_i,size_sf_f) scalled_samp_freq_in, Int#(size_s) base_freq_in) ;
			rg_interval <= interval_in;
			rg_scalled_samp_freq <= scalled_samp_freq_in;
			rg_base_freq <= base_freq_in;
			//$display($time,"Base frequency : %d, Interval angle : %d \n",base_freq_in, interval_in);
			flag <= 1;
		endmethod
		method Int#(size_a) results_base if(flag == 0);
			return rg_base_angle;
		endmethod
		method Int#(size_a) results_interval if(flag == 0);
			return rg_interval_angle;
		endmethod
	endmodule : mkfreqsynthesiser
endpackage : freqsynthesiser