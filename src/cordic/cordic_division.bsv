package cordic_division;

    import FixedPoint::*;

    interface Ifc_cordic_division#(numeric type magnitude_length,
                                   numeric type samples_interval,
                                   numeric type cordic_iterations_division);
        method Action get_inputs(Int#(magnitude_length) e,Int#(magnitude_length) l);
        method Int#(TAdd#(TLog#(samples_interval),2)) result;
    endinterface : Ifc_cordic_division
    module mkcordic_division(Ifc_cordic_division#(magnitude_length,samples_interval,cordic_iterations_division) ifc_division);

        Bit#(TAdd#(TLog#(cordic_iterations_division),1)) cordic_iter = fromInteger(valueOf(cordic_iterations_division));
        Int#(TAdd#(TLog#(samples_interval),2)) hspcc = fromInteger(valueOf(samples_interval));

        Reg#(FixedPoint#(magnitude_length,16)) rg_dividend <- mkReg(0);
        Reg#(FixedPoint#(magnitude_length,16)) rg_divisor <- mkReg(0);
        Reg#(FixedPoint#(2,14)) rg_quotient <- mkReg(0);
        Reg#(Int#(TAdd#(TLog#(samples_interval),2))) rg_code_error_out <- mkReg(0);
        Reg#(Bit#(1)) rg_flag <- mkReg(0);

        Reg#(Bit#(TAdd#(TLog#(cordic_iterations_division),1))) rg_count <- mkReg(cordic_iter+1);

        rule r1_cordic_iterations(rg_count < cordic_iter);
            if(rg_dividend == 0)
                rg_count <= cordic_iter;
            else
            begin
                if(((rg_dividend > 0)&&(rg_divisor > 0))||((rg_dividend < 0)&&(rg_divisor < 0)))
                begin
                    rg_dividend <= rg_dividend - (rg_divisor >> rg_count);
                    rg_quotient <= rg_quotient + (1 >> rg_count);
                end
                else
                begin
                    rg_dividend <= rg_dividend + (rg_divisor >> rg_count);
                    rg_quotient <= rg_quotient - (1 >> rg_count);
                end
                rg_count <= rg_count + 1;
            end
            //$display($time," %d : %d/%d ", rg_count,rg_dividend,rg_divisor);
            //fxptWrite(10,rg_quotient);
        endrule
        rule r2_factor_multiplication((rg_count == cordic_iter)&&(rg_flag == 0));
            FixedPoint#(TAdd#(TLog#(samples_interval),2),2) temp = fromInt(hspcc);
            FixedPoint#(TAdd#(TLog#(samples_interval),4),16) mult = fxptMult(rg_quotient,temp);
            //$display($time," output : ");
            //fxptWrite(10,mult);
            rg_code_error_out <= truncate(fxptGetInt(mult));
            rg_flag <= 1;
        endrule
        method Action get_inputs(Int#(magnitude_length) e,Int#(magnitude_length) l);
            rg_dividend <= fromInt(e - l);
            rg_divisor  <= fromInt(e + l);
            rg_quotient <= 0;
            rg_count <= 1;
            rg_flag <= 0;
            //$display($time," Cordic division inputs : e = %d, l=%d and hspcc = %d \n",e,l,hspcc);
            //$display($time,"Inputs = %d & %d \n",e,l);
        endmethod
        method Int#(TAdd#(TLog#(samples_interval),2)) result if(rg_flag == 1);
            return rg_code_error_out;
        endmethod
    endmodule : mkcordic_division
endpackage : cordic_division