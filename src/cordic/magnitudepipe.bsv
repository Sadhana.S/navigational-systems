/*
Module : Magnitude computation using CORDIC
Description : 
Input : Results from correlation of In_phase(I) and Quadrature Phase(Q)
Output : sqrt(I^2 + Q^2)*1.64676
    The constant 1.64676 is introduced by the cordic iterations. The constant can be ignored as the output is obtained by the ratio(first_max/second_max) and gets cancelled
*/
package magnitudepipe;

import Vector::*;
import FIFO::*;
import SpecialFIFOs::*;

interface Ifc_magnitudepipe#(numeric type magnitude_cordic_iterations,
                             numeric type magnitude_scalling_in_processing,
                             numeric type magnitude_length);
    method Action get_inputs(Int#(magnitude_length) i_in, Int#(magnitude_length) q_in);
    method ActionValue#(Int#(magnitude_length)) result_magnitude();
endinterface : Ifc_magnitudepipe

module mkmagnitudepipe(Ifc_magnitudepipe#(magnitude_cordic_iterations,
                                          magnitude_scalling_in_processing,
                                          magnitude_length))
                                          provisos(Add#(1,magnitude_cordic_iterations,magnitude_cordic_pipe_length),
                                                   Add#(magnitude_length,magnitude_scalling_in_processing,a));

    Bit#(TLog#(magnitude_cordic_pipe_length)) cordic_iter = fromInteger(valueOf(magnitude_cordic_iterations));

    Vector#(magnitude_cordic_pipe_length,FIFO#(Tuple2 #(Int#(TAdd#(magnitude_length,magnitude_scalling_in_processing)),Int#(TAdd#(magnitude_length,magnitude_scalling_in_processing))))) processing_pipe <- replicateM(mkPipelineFIFO);

    for(Bit#(TLog#(magnitude_cordic_pipe_length)) j = 0; j < cordic_iter; j = j+1)
    begin
        rule r1_cordic_processing;
            match{.magnitude,.y} = processing_pipe[j].first; processing_pipe[j].deq;
            //$display($time," %d Intermediate output : %d and %d \n",j,magnitude,y);
            let a = magnitude >> j;
            let b = y >> j;
            processing_pipe[j+1].enq(tuple2((y<0)?magnitude-b:magnitude+b,(y<0)?y+a:y-a));
        endrule
    end

    method Action get_inputs(Int#(magnitude_length) i_in, Int#(magnitude_length) q_in);
        processing_pipe[0].enq(tuple2((i_in<0)?signExtend(-i_in):signExtend(i_in),(i_in<0)?signExtend(-q_in):signExtend(q_in)));
    endmethod
    method ActionValue#(Int#(magnitude_length)) result_magnitude();
        match{.mag_out,.y_out} = processing_pipe[cordic_iter].first; processing_pipe[cordic_iter].deq;
        Int#(magnitude_length) magnitude = truncate(mag_out);
        return magnitude;
    endmethod
endmodule : mkmagnitudepipe
endpackage : magnitudepipe