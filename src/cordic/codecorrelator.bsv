/*
Module : code shift correlator
Description:
Input : Frequency shifted I and Q samples from cordic_accumulation which is stored in BRAM and the PRN number which will be an offset in prn_value BRAMLoad text file
Output: The first and second maximum along with their code phases.

1. For 'n' parallel correlators, 2048/n(~2046/n) systolic iterations are done
2. Each Systolic iteration have initial PRN fetch (n fetches) and 2046 processing cycles where one input sample at a time is given every clock cycle
3. At the end of each systolic iteration, 'n' correlation outputs (I and Q) (result of n - PRN shifts) will be obtained which is then sent to the pipelined magnitude computation (sqrt(I^2+Q^2))
4. After the initial latency of magnitude computator, the outputs are obtained for every clock cycle which are compared to update the first, second and third maximum
*/

package codecorrelator;

import magnitudepipe::*;
import cordicpipe_accumulation::*;
import Vector::*;
import BRAMCore::*;

interface Ifc_codecorrelator#(numeric type angle_length,
                              numeric type accumulated_length,
                              numeric type samples,
                              numeric type cordic_iterations,
                              numeric type input_length,
                              numeric type frequency_comp_length,
                              numeric type common_hspcc,
                              numeric type different_hspcc,
                              numeric type cordic_additional_scalling,
                              numeric type max_SPCC,
                              numeric type max_PRN,
                              numeric type magnitude_cordic_iterations,
                              numeric type magnitude_scalling_in_processing,
                              numeric type magnitude_length,
                              numeric type samples_correlated,
                              numeric type systolic_cells,
                              numeric type systolic_iterations);
    method Action initialise(Bit#(TLog#(max_PRN)) prn_no);
    method Int#(magnitude_length) result_firstmax();
    method Bit#(TLog#(samples_correlated)) result_firstmax_code();
    method Int#(magnitude_length) result_secondmax();
    method Bit#(TLog#(samples_correlated)) result_secondmax_code();
endinterface : Ifc_codecorrelator

module mkcodecorrelator(BRAM_PORT#(Bit#(TAdd#(TLog#(max_PRN),10)),Bit#(1)) prn_val,
                        Ifc_cordicpipe_accumulation#(input_length,frequency_comp_length,samples_correlated,accumulated_length,max_SPCC,common_hspcc,different_hspcc) cordic_accumulation,
                        Ifc_magnitudepipe#(magnitude_cordic_iterations,magnitude_scalling_in_processing,magnitude_length) magnitudepipe,
                        Ifc_codecorrelator#(angle_length,accumulated_length,samples,cordic_iterations,input_length,frequency_comp_length,
                                            common_hspcc,different_hspcc,cordic_additional_scalling,max_SPCC,max_PRN,magnitude_cordic_iterations,
                                            magnitude_scalling_in_processing,magnitude_length,samples_correlated,systolic_cells,systolic_iterations) ifc_code_corr)
                        provisos(Add#(TLog#(max_SPCC),TAdd#(input_length,frequency_comp_length),accumulated_length),
                                 Add#(TLog#(samples_correlated),accumulated_length,magnitude_length),
                                 Add#(b,TLog#(samples),36)
                                 );

    Bit#(TAdd#(TLog#(samples_correlated),1)) samp_ref = fromInteger(valueOf(samples_correlated));
    Bit#(TLog#(systolic_iterations)) sys_iter = fromInteger(valueOf(systolic_iterations)-1);
    Bit#(TAdd#(TLog#(systolic_cells),1)) sys_cells = fromInteger(valueOf(systolic_cells)-1);

    Reg#(Bit#(3)) rg_flag <- mkReg(0);//for the state transition in FSM
    Reg#(Bit#(1)) rg_writeflag <- mkReg(0);//for the start an stop of writing values to magnitude computation pipe
    Reg#(Bit#(TAdd#(TLog#(systolic_cells),1))) rg_count_initial <- mkReg(0);//
    Reg#(Bit#(TLog#(systolic_iterations))) rg_count_super <- mkReg(0);
    Reg#(Bit#(TAdd#(TLog#(systolic_cells),1))) rg_count_write <- mkReg(0);
    Reg#(Bit#(TLog#(samples_correlated))) rg_count_compare <- mkReg(0);

    Reg#(Bit#(TAdd#(TLog#(max_PRN),10))) rg_initial_prn <- mkReg(0);
    Reg#(Bit#(TAdd#(TLog#(max_PRN),10))) rg_current_prn <- mkReg(0);
    Reg#(Bit#(TAdd#(TLog#(max_PRN),10))) rg_last_prn <- mkReg(0);

    Reg#(Int#(magnitude_length)) rg_current_I <- mkReg(0);
    Reg#(Int#(magnitude_length)) rg_current_Q <- mkReg(0);

    Vector#(systolic_cells,Reg#(Int#(magnitude_length))) accumulation_I_cell <- replicateM(mkReg(0));
    Vector#(systolic_cells,Reg#(Int#(magnitude_length))) accumulation_Q_cell <- replicateM(mkReg(0));
    Vector#(systolic_cells,Reg#(Bit#(1))) prn_cell <- replicateM(mkReg(0));

    Reg#(Bit#(TAdd#(TLog#(samples_correlated),1))) rg_count_iter <- mkReg(2);

    Reg#(Int#(magnitude_length)) rg_firstmax <- mkReg(0);
    Reg#(Bit#(TLog#(samples_correlated))) rg_firstmax_code <- mkReg(0);
    Reg#(Int#(magnitude_length)) rg_secondmax <- mkReg(0);
    Reg#(Bit#(TLog#(samples_correlated))) rg_secondmax_code <- mkReg(0);
    Reg#(Int#(magnitude_length)) rg_thirdmax <- mkReg(0);
    Reg#(Bit#(TLog#(samples_correlated))) rg_thirdmax_code <- mkReg(0);

		//Initial PRN fetch for 64 cells
    rule r1_initialise_prn_cells(rg_flag == 1);
        prn_cell[rg_count_initial] <= prn_val.read;
        prn_cell[rg_count_initial+1] <= prn_val.read;
        //$display($time,"%d %d prn = %d",rg_count_super,rg_count_initial,rg_initial_prn);
        prn_val.put(False,rg_initial_prn,?);
        if(rg_initial_prn == rg_last_prn)
            rg_initial_prn <= rg_last_prn - 1022;
        else
            rg_initial_prn <= rg_initial_prn + 1;
        if(rg_count_initial == sys_cells-1)
        begin
            rg_flag <= 2;
            rg_current_prn <= rg_initial_prn;
            let i <- cordic_accumulation.accumulated_i_out;
            let q <- cordic_accumulation.accumulated_q_out;
            rg_current_I <= signExtend(i);
            rg_current_Q <= signExtend(q);
            cordic_accumulation.bram_address(1);
            rg_count_initial <= 0;
            //$display($time," %d Initialisation of PRN values done\n",rg_count_super);
        end
        else
            rg_count_initial <= rg_count_initial + 2;
    endrule
    //Systolic processing which is repeated for 2046 cycles (size of input)
    for(Bit#(TAdd#(TLog#(systolic_cells),1)) i=0; i<=sys_cells; i=i+1)
    begin
        rule r2_processing_iterations((rg_flag == 2)&&(rg_writeflag == 0));
            if(prn_cell[i] == 1)
            begin
                accumulation_I_cell[i] <= accumulation_I_cell[i] + rg_current_I;
                accumulation_Q_cell[i] <= accumulation_Q_cell[i] + rg_current_Q;
            end
            else
            begin
                accumulation_I_cell[i] <= accumulation_I_cell[i] - rg_current_I;
                accumulation_Q_cell[i] <= accumulation_Q_cell[i] - rg_current_Q;
            end
            if(i!=sys_cells)
                prn_cell[i] <= prn_cell[i+1];
        endrule
    end
    //PRN fetch for the last systolic cell for every iteration
    rule r3_prn_update((rg_flag == 2)&&(rg_writeflag == 0));
        prn_cell[sys_cells] <= prn_val.read;
        let i <- cordic_accumulation.accumulated_i_out;
        let q <- cordic_accumulation.accumulated_q_out;
        rg_current_I <= signExtend(i);
        rg_current_Q <= signExtend(q);
        let temp_1 = rg_current_prn + zeroExtend(rg_count_iter[0]);
        if(temp_1 == rg_last_prn + 1)
        begin
            Bit#(TAdd#(TLog#(max_PRN),10)) temp_2 = rg_last_prn -1022;
            rg_current_prn <= temp_2;
            //$display($time,"%d %d prn = %d",rg_count_super,rg_count_iter,temp_2);
            prn_val.put(False,temp_2,?);
        end
        else
        begin
            rg_current_prn <= temp_1;
            //$display($time,"%d %d prn = %d",rg_count_super,rg_count_iter,temp_1);
            prn_val.put(False,temp_1,?);
        end
        if(rg_count_iter == samp_ref)
        begin
            rg_flag <= 3;
            rg_writeflag <= 1;
            //$display($time," %d Systolic processing is done\n",rg_count_super);
        end
        else
        begin
            cordic_accumulation.bram_address(truncate(rg_count_iter));
            rg_count_iter <= rg_count_iter + 1;
        end
    endrule
    //At the end of 64 correlations, I and Q values of each systolic cell is sent to pipelined magnitude computation
    rule r4_magnitude_computation(rg_writeflag == 1);
        magnitudepipe.get_inputs(accumulation_I_cell[rg_count_write],accumulation_Q_cell[rg_count_write]);
        //$display($time,"%d %d : I=%d and Q = %d",rg_count_super,rg_count_write,accumulation_I_cell[rg_count_write],accumulation_Q_cell[rg_count_write]);
        accumulation_I_cell[rg_count_write] <= 0;
        accumulation_Q_cell[rg_count_write] <= 0;
        if(rg_count_write == sys_cells)
        begin
            rg_writeflag <= 0;
            rg_count_write <= 0;
            //$display($time," %d Inputs are given for magnitude computation\n",rg_count_super);
        end
        else
            rg_count_write <= rg_count_write + 1;
    endrule
    //Compare the 64 magnitudes and update first, second and third maximum
    rule r5_compare_magnitude;
        Int#(magnitude_length) a <- magnitudepipe.result_magnitude();
        //$display($time," %d %d \n",rg_count_compare,a);
        if(a>rg_firstmax)
        begin
            rg_firstmax <= a;
            rg_firstmax_code <= rg_count_compare;
            rg_secondmax <= rg_firstmax;
            rg_secondmax_code <= rg_firstmax_code;
            rg_thirdmax <= rg_secondmax;
            rg_thirdmax_code <= rg_secondmax_code;
        end
        else if(a>rg_secondmax)
        begin
            rg_secondmax <= a;
            rg_secondmax_code <= rg_count_compare;
            rg_thirdmax <= rg_thirdmax;
            rg_thirdmax_code <= rg_thirdmax_code;
        end
        else if(a>rg_thirdmax)
        begin
            rg_thirdmax <= a;
            rg_thirdmax_code <= rg_count_compare;
        end
        rg_count_compare <= rg_count_compare + 1;
        if(rg_count_compare == truncate(samp_ref - 1))
        begin
            rg_flag <= 5;
            //$display($time," %d Magnitude comparison is done\n",rg_count_super);
        end
    endrule
    // reinitialisation for next systolic prn fetch and processing
    rule r6_reset(rg_flag == 3);
        if(rg_count_super == sys_iter)
            rg_flag <= 4;
        else
        begin
            cordic_accumulation.bram_address(0);
            rg_count_initial <= 0;
            rg_count_iter <= 2;
            rg_count_super <= rg_count_super + 1;
            rg_flag <= 1; 
            //$display($time," %d Re-initialisation\n",rg_count_super);
        end
    endrule
    //As first and second maximum should not belong to same PRN, if the first and second maximum codes differs by 1, then the second maximum is updated by the third one 
    rule r7_max_decision(rg_flag == 5);
        if((rg_firstmax_code - rg_secondmax_code == 1)||(rg_secondmax_code - rg_firstmax_code == 1))
        begin
            rg_secondmax <= rg_thirdmax;
            rg_secondmax_code <= rg_thirdmax_code;
        end
        rg_flag <= 6;
    endrule

    method Action initialise(Bit#(TLog#(max_PRN)) prn_no);
        //$display($time, "INITIALISE code correlator\n");
        rg_flag <= 1;
        rg_count_super <= 0;
        rg_count_initial <= 0;
        rg_count_iter <= 2;
        rg_count_compare <= 0;
        Bit#(TAdd#(TLog#(max_PRN),10)) temp = zeroExtend(prn_no)*1024;
        //$display($time,"start prn = %d",temp);
        prn_val.put(False,temp,?);
        cordic_accumulation.bram_address(0);
        rg_initial_prn <= temp + 1;
        rg_last_prn <= temp + 1022;
        rg_firstmax <= 0;
        rg_firstmax_code <= 0;
        rg_secondmax <= 0;
        rg_secondmax_code <= 0;
        rg_thirdmax <= 0;
        rg_thirdmax_code <= 0;
    endmethod
    method Int#(magnitude_length) result_firstmax() if(rg_flag == 6);
        return rg_firstmax;
    endmethod
    method Bit#(TLog#(samples_correlated)) result_firstmax_code() if(rg_flag == 6);
        return rg_firstmax_code;
    endmethod
    method Int#(magnitude_length) result_secondmax() if(rg_flag == 6);
        return rg_secondmax;
    endmethod
    method Bit#(TLog#(samples_correlated)) result_secondmax_code() if(rg_flag == 6);
        return rg_secondmax_code;
    endmethod

endmodule : mkcodecorrelator

endpackage : codecorrelator