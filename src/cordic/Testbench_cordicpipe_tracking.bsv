import cordicpipe::*;

typedef 12 Samples;

module mkTestbench();

    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);

    Reg#(Bit#(3)) rg_flag <- mkReg(0);
    Reg#(Bit#(16)) rg_count <- mkReg(0);

    Ifc_cordicpipe cordicpipe <- mkcordicpipe;

    rule r1(rg_flag == 0);
        cordicpipe.initialise(1276024046,2);
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        let a = cordicpipe.cosine;
        let b = cordicpipe.sine;
        $display($time,"\n %d Cosine = %d and Sine =%d",rg_count,a,b);
        rg_count <= rg_count + 1;
    endrule
    rule r3(rg_count == samp);
        $finish;
    endrule

endmodule : mkTestbench