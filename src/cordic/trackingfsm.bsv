/*
Module name: Tracking FSM
Description: This module performs tracking of a satellite that has been acquired. In tracking, we
			 perform fine tuning of the code and freq. shifts. Fine tuned code shifting is done by
			 performing correlation on early, late and prompt code shifts. Fine tuned freq. shift is
			 done using a freq discriminator.
*/
/*
1) Prompt Inphase and Quadrature phase components in BRAM
2) 
*/
package trackingfsm;

import cordicpipe_main::*;
import elp_correlator::*;
import arctan::*;
import phasefilter::*;
import freqdiscriminator::*;
import freqfilter::*;
import cordic_division::*;
import BRAMCore::*;

interface Ifc_trackingfsm#(numeric type samples,
                           numeric type input_length,
                           numeric type frequency_component_length,
                           numeric type cordic_iterations,
                           numeric type cordic_additional_scalling,
                           numeric type angle_length,
                           numeric type magnitude_length,
                           numeric type max_prn,
                           numeric type ms_of_data_to_be_processed,
                           numeric type max_spcc,
                           numeric type min_hspcc,
                           numeric type even_spcc,
                           numeric type odd_spcc,
                           numeric type cordic_additional_scalling_arctan,
                           numeric type cordic_iterations_arctan,
                           numeric type cordic_iterations_division
                           );
    method Action get_inputs(Bit#(10) code_shift_prn, Bit#(TLog#(max_spcc)) code_shift_sample, Int#(angle_length) carr_freq, Int#(angle_length) carr_phase, Bit#(1) control_bit, Bit#(TLog#(max_prn)) prn_no, Bit#(TLog#(ms_of_data_to_be_processed)) loop_cnt);
    method Bit#(10) code_shift_prn_out;
    method Bit#(TLog#(max_spcc)) code_shift_sample_out;
    method Int#(angle_length) carr_freq_out;
    method Int#(angle_length) carr_phase_out;
    method Int#(magnitude_length) prompt_inphase;
    method Int#(magnitude_length) prompt_quadphase;
    method Bit#(1) sign_change;
endinterface : Ifc_trackingfsm

module mktrackingfsm(BRAM_PORT#(Bit#(36),Int#(input_length)) i_val,
                     BRAM_PORT#(Bit#(36),Int#(input_length)) q_val,
                     BRAM_PORT#(Bit#(TAdd#(TLog#(max_prn),10)),Bit#(1)) prn_val,
                     BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                     Int#(2) kp1, Int#(2) kp2,
                     Int#(16) kf1, Int#(16) kf2,
                     Ifc_trackingfsm#(samples,input_length,frequency_component_length,cordic_iterations,cordic_additional_scalling,
                                      angle_length,magnitude_length,max_prn,ms_of_data_to_be_processed,max_spcc,min_hspcc,even_spcc,odd_spcc,
                                      cordic_additional_scalling_arctan,cordic_iterations_arctan,cordic_iterations_division) ifc_tracking)
                     provisos(Add#(a,TAdd#(TLog#(min_hspcc),2),TAdd#(TLog#(max_spcc),1)),
                              Add#(b,16,angle_length),
                              Add#(c,TAdd#(TLog#(samples),1),angle_length),
                              Add#(d,TAdd#(input_length,frequency_component_length),magnitude_length),
                              Add#(e,TLog#(samples),36),
                              Add#(f,TLog#(ms_of_data_to_be_processed),36)
                              );
    Reg#(Bit#(4)) rg_flag <- mkReg(0);

    Reg#(Bit#(10)) rg_code_shift_prn <- mkReg(0);
    Reg#(Bit#(TLog#(max_spcc))) rg_code_shift_sample <- mkReg(0);
    Reg#(Int#(angle_length)) rg_carr_freq <- mkReg(0);
    Reg#(Int#(angle_length)) rg_carr_phase <- mkReg(0);
    Reg#(Bit#(1)) rg_control_bit <- mkReg(1);
    Reg#(Bit#(TLog#(ms_of_data_to_be_processed))) rg_loopcnt <- mkReg(0);
    Reg#(Bit#(TLog#(max_prn))) rg_prn_no <- mkReg(0);

    Reg#(Int#(angle_length)) rg_phasedev1 <- mkReg(0);
    Reg#(Int#(angle_length)) rg_phasedev2 <- mkReg(0);
    Reg#(Int#(magnitude_length)) rg_IP <- mkReg(0);
    Reg#(Int#(magnitude_length)) rg_QP <- mkReg(0);
    Reg#(Int#(angle_length)) rg_carr_freqdev <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_change <- mkReg(0);


    Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_component_length,cordic_additional_scalling,ms_of_data_to_be_processed) cordicpipe <- mkcordicpipe_main(i_val,q_val);
    Ifc_elp_correlator#(samples,input_length,frequency_component_length,angle_length,magnitude_length,ms_of_data_to_be_processed,max_spcc,max_prn,even_spcc,odd_spcc) elp_correlator <- mkelp_correlator(fromInteger(valueOf(min_hspcc)),prn_val,prn_val_sample,cordicpipe);
    Ifc_arctan#(angle_length,magnitude_length,cordic_additional_scalling_arctan,cordic_iterations_arctan) arctan_e <- mkarctan;
    Ifc_arctan#(angle_length,magnitude_length,cordic_additional_scalling_arctan,cordic_iterations_arctan) arctan_p <- mkarctan;
    Ifc_arctan#(angle_length,magnitude_length,cordic_additional_scalling_arctan,cordic_iterations_arctan) arctan_l <- mkarctan;
    Ifc_phasefilter#(angle_length) phasefilter <- mkphasefilter(kp1,kp2);
    Ifc_freqdiscriminator#(angle_length,samples) freqdiscriminator <- mkfreqdiscriminator;
    Ifc_freqfilter#(angle_length) freqfilter <- mkfreqfilter(kf1,kf2);
    Ifc_cordic_division#(magnitude_length,min_hspcc,cordic_iterations_division) cordic_division <- mkcordic_division;

    /*rule r1_angle_calculation_to_cordic(rg_flag == 1);
        //Input to Frequency synthesiser to get the scalled frequency value
        //freqsynthesiser.get_inputs(0,357.91394133333,rg_carr_freq,rg_carr_phase);//for sampling frequency 12 MHz
        freqsynthesiser.get_inputs(0,262.406662919426,rg_carr_freq);//for sampling frequency 16367600 Hz
        rg_flag <= 2;
    endrule*/
    rule r2_cordic_initialise(rg_flag == 2);
        //Output from frequency synthesiser
        //let a = freqsynthesiser.results_base();
        //Input to cordicpipe via multiply and add
        cordicpipe.initialise(signExtend(rg_carr_freq),0,rg_carr_phase,rg_loopcnt);
        $display($time,"Initialised_cordic \n");
        rg_flag <= 3;
    endrule
    rule r3_early_late_prompt_correlation_initialise(rg_flag == 3);//need lots of change here
        elp_correlator.get_codeshift(rg_code_shift_prn,rg_code_shift_sample,rg_prn_no);
        $display($time,"Initialise code shift \n");
        rg_flag <= 4;
    endrule
    rule r4_early_late_prompt_correlation_values(rg_flag == 4);
        //Outputs from multiply and add module 
        let ie = elp_correlator.result_ie;
        let qe = elp_correlator.result_qe;
        let temp_IP = elp_correlator.result_ip;
        let temp_QP = elp_correlator.result_qp;
        let il = elp_correlator.result_il;
        let ql = elp_correlator.result_ql;
        let temp_phase = cordicpipe.lastphase_out;
        //let temp_phase = 0;
        rg_carr_phase <= temp_phase;
        rg_IP <= temp_IP;
        rg_QP <= temp_QP;
        //Sign change computation from the Prompt Inphase correlation value
        rg_sign_change <= rg_sign_change ^ pack(temp_IP)[valueOf(TSub#(magnitude_length,1))];
        //Inphase and Quadrature phase correlation values as inputs to arctan module
        arctan_e.get_inputs(ie,qe);//Early
        arctan_p.get_inputs(temp_IP,temp_QP);//Prompt
        arctan_l.get_inputs(il,ql);//Late
        rg_flag <= 5;
        $display($time," From Multadd : ie = %d, qe = %d, ip = %d, qp = %d, il = %d, ql = %d, phase = %d",ie,qe,temp_IP,temp_QP,il,ql,temp_phase);
    endrule
    rule r5_correlation_magnitudes(rg_flag == 5);
        //Early and Late magnitudes from respective arctan modules
        let e = arctan_e.result_magnitude;
        let l = arctan_l.result_magnitude;
        //Early and Late magnitudes as inputs to code disciminator
        cordic_division.get_inputs(l,e);
        //Prompt phase from arctan module
        rg_phasedev1 <= arctan_p.result_phase;
        rg_flag <= 6;
        $display($time," From arctan : E = %d, L = %d \n",e,l);
    endrule
    rule r6_phase_filter_and_frequency_discriminator((rg_flag == 6)&&(rg_control_bit == 1));
        //Present and previous phase values as inputs to phase filter
        phasefilter.get_inputs(rg_phasedev1,rg_phasedev2);
        //Integration time, present and previous phase values to frequency discriminator
        freqdiscriminator.get_inputs(rg_phasedev1,rg_phasedev2);
         //Code discriminator result
         Int#(TAdd#(TLog#(min_hspcc),2)) temp = cordic_division.result;
         //Updating code shift from code discriminator output
         elp_correlator.code_error_correction(rg_code_shift_prn,rg_code_shift_sample,signExtend(temp),rg_prn_no);
         $display($time," From code discriminator = %d \n",temp);
         //Updating carrier phase error to a register for next tracking iteration
        rg_flag <= 7;
        $display($time," From arctan : phase error = %d and previous phase error = %d \n",rg_phasedev1,rg_phasedev2);
    endrule
    rule r7_frequency_filter(rg_flag == 7);
        //Output from phase filter
        let temp_phase = phasefilter.results;
        rg_phasedev1 <= temp_phase;
        //Output from frequency discriminator
        let temp_freqdev = freqdiscriminator.results;
        //Frequency deviation from frequenct discriminator and previous filtered frequency deviation as inputs to frequency filter
        freqfilter.get_inputs(temp_freqdev, rg_carr_freqdev);//Frequency filter input
        //multi-path mitigation module comes here
        Bit#(10) temp_prn = elp_correlator.corrected_code_prn;
        rg_code_shift_prn <= temp_prn;
        Bit#(TLog#(max_spcc)) temp_sample = elp_correlator.corrected_code_sample;
        rg_code_shift_sample <= temp_sample;
        $display($time," Corrected code shift_prn = %d and sample = %d \n",temp_prn,temp_sample);
        rg_flag <= 8;
        $display($time," From phase filter : Filtered phase = %d \n",temp_phase);
        $display($time," From Frequency discriminator : frequency error = %d \n",temp_freqdev);
    endrule
    rule r8_phase_and_frequency_update(rg_flag == 8);
        //Output from frequency filter
        let temp_freqdev = freqfilter.results;
        //Updating Carrier frequency from frequency filter output
        rg_carr_freq <= rg_carr_freq + temp_freqdev;
        /*Int#(32) temp_freq = rg_carr_freq + temp_freqdev;
        if(temp_freq < 0) 
            rg_carr_freq <= temp_freq + 4294967295;
        else
            rg_carr_freq <= temp_freq;*/
        //Updating carrier phase from phase filter output
        rg_carr_phase <= rg_carr_phase + rg_phasedev1;
        /*Int#(32) temp_phase = rg_carr_phase + rg_phasedev1;
        if(temp_phase < 0)
            rg_carr_phase <= temp_phase + 4294967295;
        else
            rg_carr_phase <= temp_phase;*/
        //Updating carrier frequency error to a register for next tracking itertaion
        rg_carr_freqdev <= temp_freqdev;
        rg_flag <= 6;
        rg_control_bit <= 0;
        $display($time," From frequency filter : filtered frequency = %d \n",temp_freqdev);
    endrule
    rule r9_code_discriminator_output((rg_flag == 6)&&(rg_control_bit == 0));
        //Updating carrier phase error to a register for next tracking iteration
        rg_phasedev2 <= rg_phasedev1;
        rg_flag <= 9;
        $display($time," Corrected phase = %d \n",rg_carr_phase);
        $display($time," Corrected frequency = %d \n",rg_carr_freq);
    endrule
    rule r10_code_filter(rg_flag == 9);
        
        rg_flag <= 10;
    endrule

    method Action get_inputs(Bit#(10) code_shift_prn, Bit#(TLog#(max_spcc)) code_shift_sample, Int#(angle_length) carr_freq, Int#(angle_length) carr_phase, Bit#(1) control_bit, Bit#(TLog#(max_prn)) prn_no, Bit#(TLog#(ms_of_data_to_be_processed)) loop_cnt);
        rg_code_shift_prn <= code_shift_prn;
        rg_code_shift_sample <= code_shift_sample;
        rg_carr_freq <= carr_freq;
        rg_carr_phase <= carr_phase;
        rg_control_bit <= control_bit;
        rg_loopcnt <= loop_cnt;
        rg_sign_change <= pack(rg_IP)[valueOf(TSub#(magnitude_length,1))];
        rg_prn_no <= prn_no;
        rg_flag <= 2;
    endmethod
    method Bit#(10) code_shift_prn_out if(rg_flag == 10);
        return rg_code_shift_prn;
    endmethod
    method Bit#(TLog#(max_spcc)) code_shift_sample_out if(rg_flag == 10);
        return rg_code_shift_sample;
    endmethod
    method Int#(angle_length) carr_freq_out if(rg_flag == 10);
        return rg_carr_freq;
    endmethod
    method Int#(angle_length) carr_phase_out if(rg_flag == 10);
        return rg_carr_phase;
    endmethod
    method Int#(magnitude_length) prompt_inphase if(rg_flag == 10);
        return rg_IP;
    endmethod
    method Int#(magnitude_length) prompt_quadphase if(rg_flag == 10);
        return rg_QP;
    endmethod
    method Bit#(1) sign_change if(rg_flag == 10);
        return rg_sign_change;
    endmethod

endmodule : mktrackingfsm

endpackage : trackingfsm