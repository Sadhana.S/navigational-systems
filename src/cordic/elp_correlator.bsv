/*
Module: Early, Late and Prompt correlator
Description: This module computes the early, prompt and late code shift correlation. Here, the three
			 code shifted samples are read and then 
TODO Is prompt really required? Instead from coarse acquisition, pass on the value of prompt corr.
*/
/*
Change PRN addressing from Int to Bit
Add PRN number as input
*/
package elp_correlator;

    import BRAMCore::*;
    import cordicpipe_main::*;

    interface Ifc_elp_correlator#(numeric type samples,
                           numeric type input_length,
                           numeric type frequency_component_length,
                           numeric type angle_length,
                           numeric type magnitude_length,
                           numeric type ms_of_data_processed,
                           numeric type max_spcc,
                           numeric type max_prn,
                           numeric type even_spcc,
                           numeric type odd_spcc
                           );
        method Action get_codeshift(Bit#(10) code_shift_prn, Bit#(TLog#(max_spcc)) code_shift_sample, Bit#(TLog#(max_prn)) prn_no);
        method Int#(magnitude_length) result_ie;
        method Int#(magnitude_length) result_qe;
        method Int#(magnitude_length) result_ip;
        method Int#(magnitude_length) result_qp;
        method Int#(magnitude_length) result_il;
        method Int#(magnitude_length) result_ql;
        method Action code_error_correction(Bit#(10) code_shift_prn,Bit#(TLog#(max_spcc)) code_shift_sample, Int#(TAdd#(TLog#(max_spcc),1)) code_error, Bit#(TLog#(max_prn)) prn_no);
        method Bit#(10) corrected_code_prn;
        method Bit#(TLog#(max_spcc)) corrected_code_sample;
    endinterface : Ifc_elp_correlator

    module mkelp_correlator(Bit#(TLog#(max_spcc)) hspcc,
                     BRAM_PORT#(Bit#(TAdd#(TLog#(max_prn),10)),Bit#(1)) prn_val,
                     BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                     Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_component_length,cordic_additional_scalling,ms_of_data_processed) cordicpipe,
                     Ifc_elp_correlator#(samples,input_length,frequency_component_length,angle_length,magnitude_length,ms_of_data_processed,max_spcc,max_prn,even_spcc,odd_spcc) ifc_multiplyadd)
                     provisos(Add#(a,TAdd#(input_length,frequency_component_length),magnitude_length),
                              Add#(1,TLog#(max_spcc),TAdd#(TLog#(max_spcc),1)),
                              Add#(b,TLog#(ms_of_data_processed),36),
                              Add#(c,TLog#(samples),36)
                              );

        Reg#(Int#(magnitude_length)) rg_IE <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_QE <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_IP <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_QP <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_IL <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_QL <- mkReg(0);

        Reg#(Bit#(TLog#(samples))) rg_accumulate_count <- mkReg(0);
        Reg#(Bit#(1)) rg_prompt_prn <- mkReg(0);
        Reg#(Bit#(1)) rg_early_prn <- mkReg(0);
        Reg#(Bit#(1)) rg_late_prn <- mkReg(0);

        Reg#(Bit#(TAdd#(TLog#(max_prn),10))) rg_start_address <- mkReg(0);
        Reg#(Bit#(TAdd#(TLog#(max_prn),10))) rg_stop_address <- mkReg(0);
        Reg#(Bit#(TAdd#(TLog#(max_prn),10))) rg_prn_address_l <- mkReg(0);
        Reg#(Bit#(1)) rg_prompt_set <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_sample_count_e <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_sample_count_p <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_sample_count_l <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_early_sample_ref <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_prompt_sample_ref <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_late_sample_ref <- mkReg(0);
        Reg#(Bit#(3)) rg_readFlag <- mkReg(0);

        Reg#(Bit#(10)) rg_corrected_code_prn <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_corrected_code_sample <- mkReg(0);
        Reg#(Int#(TAdd#(TLog#(max_spcc),1))) rg_code_error <- mkReg(0);

        rule r1_early_late_computations(rg_readFlag == 1);
            Int#(TAdd#(TLog#(max_spcc),1)) sample_p = unpack(zeroExtend(rg_sample_count_p));
            Int#(TAdd#(TLog#(max_spcc),1)) sample_e = sample_p - (unpack(zeroExtend(hspcc)) - 1);
            Int#(TAdd#(TLog#(max_spcc),1)) sample_l = sample_p + (unpack(zeroExtend(hspcc)) - 1);
            $display($time," Sample_e = %d, sample_p = %d and sample_l = %d \n",sample_e,sample_p,sample_l);
            Int#(TAdd#(TLog#(max_spcc),1)) sample_p_ref;
            Bit#(TLog#(max_spcc)) current_ref;
            if(prn_val_sample.read == 1)
            begin
                sample_p_ref = fromInteger(valueOf(odd_spcc));
                current_ref = fromInteger(valueOf(odd_spcc));
            end
            else
            begin
                sample_p_ref = fromInteger(valueOf(even_spcc));
                current_ref = fromInteger(valueOf(even_spcc));
            end
            if(sample_e < 0)
            begin
                if(rg_prn_address_l == rg_start_address)
                begin
                    prn_val.put(False,rg_stop_address,?);
                    prn_val_sample.put(False,rg_stop_address[9:0],?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_address_e = rg_prn_address_l - 1;
                    prn_val.put(False,temp_address_e,?);
                    prn_val_sample.put(False,temp_address_e[9:0],?);
                end
                rg_prompt_set <= 1;
                rg_sample_count_e <= truncate(pack(0 - sample_e));

                rg_sample_count_l <= truncate(pack(sample_l));
                rg_late_sample_ref <= current_ref;
                rg_late_prn <= prn_val.read;
            end
            else if(sample_l >= sample_p_ref)
            begin
                if(rg_prn_address_l == rg_stop_address)
                begin
                    rg_prn_address_l <= rg_start_address;
                    prn_val.put(False,rg_start_address,?);
                    prn_val_sample.put(False,rg_start_address[9:0],?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_address_l = rg_prn_address_l + 1;
                    rg_prn_address_l <= temp_address_l;
                    prn_val.put(False,temp_address_l,?);
                    prn_val_sample.put(False,temp_address_l[9:0],?);
                end
                rg_prompt_set <= 0;
                rg_sample_count_l <= truncate(pack(sample_l - sample_p_ref));

                rg_sample_count_e <= truncate(pack(sample_e));  
                rg_early_sample_ref <= current_ref;
                rg_early_prn <= prn_val.read;
            end
            else
            begin
                rg_sample_count_l <= truncate(pack(sample_l));
                rg_late_sample_ref <= current_ref;
                rg_late_prn <= prn_val.read;

                rg_sample_count_e <= truncate(pack(sample_e));
                rg_early_sample_ref <= current_ref;
                rg_early_prn <= prn_val.read;  
            end
            rg_prompt_prn <= prn_val.read;
            rg_prompt_sample_ref <= current_ref;
            rg_readFlag <= 2;
        endrule
        rule r2_early_sample_update_and_next_late_prn_fetch(rg_readFlag == 2);
            Bit#(TLog#(max_spcc)) current_ref;
            if(prn_val_sample.read == 1)
                current_ref = fromInteger(valueOf(odd_spcc));
            else
                current_ref = fromInteger(valueOf(even_spcc));
            if(rg_prompt_set == 1)
            begin
                rg_sample_count_e <= current_ref - rg_sample_count_e;
                rg_early_prn <= prn_val.read;
                rg_early_sample_ref <= current_ref;
            end
            else
            begin
                rg_late_prn <= prn_val.read;
                rg_late_sample_ref <= current_ref;
            end
            Bit#(TAdd#(TLog#(max_prn),10)) temp_address_l = rg_prn_address_l + 1;
            rg_prn_address_l <= rg_prn_address_l + 1;
            prn_val.put(False,temp_address_l,?);
            prn_val_sample.put(False,temp_address_l[9:0],?);
            rg_readFlag <= 0;
        endrule
        rule r3_early_lat_prompt_correlation(rg_accumulate_count < fromInteger(valueOf(samples)));
            Int#(TAdd#(input_length,frequency_component_length)) cosine = cordicpipe.i_out;
            Int#(TAdd#(input_length,frequency_component_length)) sine = cordicpipe.q_out;
            //$display($time," %d : cosine = %d and sine =%d \n",rg_accumulate_count,cosine,sine);
            if(rg_early_prn == 1)
            begin
                rg_IE <= rg_IE + signExtend(cosine);
                rg_QE <= rg_QE + signExtend(sine);
            end
            else
            begin
                rg_IE <= rg_IE - signExtend(cosine);
                rg_QE <= rg_QE - signExtend(sine);
            end
            if(rg_prompt_prn == 1)
            begin
                rg_IP <= rg_IP + signExtend(cosine);
                rg_QP <= rg_QP + signExtend(sine);
            end
            else
            begin
                rg_IP <= rg_IP - signExtend(cosine);
                rg_QP <= rg_QP - signExtend(sine);
            end
            if(rg_late_prn == 1)
            begin
                rg_IL <= rg_IL + signExtend(cosine);
                rg_QL <= rg_QL + signExtend(sine);
            end
            else
            begin
                rg_IL <= rg_IL - signExtend(cosine);
                rg_QL <= rg_QL - signExtend(sine);
            end
            if(rg_sample_count_e == rg_early_sample_ref-1)
            begin
                rg_early_sample_ref <= rg_prompt_sample_ref;
                rg_sample_count_e <= 0;
                rg_early_prn <= rg_prompt_prn;
            end
            else
                rg_sample_count_e <= rg_sample_count_e + 1;
            if(rg_sample_count_p == rg_prompt_sample_ref-1)
            begin
                rg_prompt_sample_ref <=rg_late_sample_ref;
                rg_sample_count_p <= 0;
                rg_prompt_prn <= rg_late_prn;
            end
            else
                rg_sample_count_p <= rg_sample_count_p + 1;
            if(rg_sample_count_l == rg_prompt_sample_ref-1)
            begin
                rg_late_prn <= prn_val.read;
                if(prn_val_sample.read == 1)
                    rg_late_sample_ref <= fromInteger(valueOf(odd_spcc));
                else
                    rg_late_sample_ref <= fromInteger(valueOf(even_spcc));
                rg_sample_count_l <= 0;
                if(rg_prn_address_l == rg_stop_address)
                begin
                    rg_prn_address_l <= rg_start_address;
                    prn_val.put(False,rg_start_address,?);
                    prn_val_sample.put(False,rg_start_address[9:0],?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_address_l = rg_prn_address_l + 1;
                    rg_prn_address_l <= temp_address_l;
                    prn_val.put(False,temp_address_l,?);
                    prn_val_sample.put(False,temp_address_l[9:0],?);
                end
            end
            else
                rg_sample_count_l <= rg_sample_count_l + 1;
            //$display($time," %d : prn : %d, %d & %d ; addresses : %d ; sample : %d, %d & %d ; ref: %d, %d & %d \n",rg_accumulate_count,rg_early_prn,rg_prompt_prn,rg_late_prn,rg_prn_address_l[9:0],rg_sample_count_e,rg_sample_count_p,rg_sample_count_l,rg_early_sample_ref,rg_prompt_sample_ref,rg_late_sample_ref);
            //$display($time," %d : inputs : %d & %d accumulated_output = %d & %d \n",rg_accumulate_count,cosine,sine,rg_IP,rg_QP);    
            rg_accumulate_count <= rg_accumulate_count + 1;
        endrule
        rule r4_code_error_correction(rg_readFlag == 3);
            Bit#(TLog#(max_spcc)) current_ref;
            if(prn_val_sample.read == 1)
                current_ref = fromInteger(valueOf(odd_spcc));
            else
                current_ref = fromInteger(valueOf(even_spcc));
            Int#(TAdd#(TLog#(max_spcc),1)) offset = zeroExtend(unpack(rg_sample_count_p)) + signExtend(rg_code_error);
            if(offset >= zeroExtend(unpack(current_ref)))
            begin
                if(rg_prn_address_l == rg_stop_address)
                begin
                    rg_corrected_code_prn <= rg_start_address[9:0];
                    rg_corrected_code_sample <= truncate(pack(offset)) - current_ref;
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_corrected = rg_prn_address_l + 1;
                    rg_corrected_code_prn <= temp_corrected[9:0];
                    rg_corrected_code_sample <= truncate(pack(offset)) - current_ref;
                end
                rg_readFlag <= 5;
            end
            else if(offset < 0)
            begin
                if(rg_prn_address_l == rg_start_address)
                begin
                    rg_corrected_code_prn <= rg_stop_address[9:0];
                    prn_val_sample.put(False,rg_stop_address[9:0],?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10)) temp_address = rg_prn_address_l - 1;
                    rg_corrected_code_prn <= temp_address[9:0];
                    prn_val_sample.put(False,temp_address[9:0],?);
                end
                rg_corrected_code_sample <= truncate(pack(0 - offset));
                rg_readFlag <= 4;
            end
            else
            begin
                rg_corrected_code_prn <= rg_prn_address_l[9:0];
                rg_corrected_code_sample <= truncate(pack(offset));
                rg_readFlag <= 5;
            end
        endrule
        rule r5_code_error_correction(rg_readFlag == 4);
            Bit#(TLog#(max_spcc)) current_ref;
            if(prn_val_sample.read == 1)
                current_ref = fromInteger(valueOf(odd_spcc));
            else
                current_ref = fromInteger(valueOf(even_spcc));
            rg_corrected_code_sample <= current_ref - rg_corrected_code_sample;
            rg_readFlag <= 5;
        endrule
        
        method Action get_codeshift(Bit#(10) code_shift_prn, Bit#(TLog#(max_spcc)) code_shift_sample, Bit#(TLog#(max_prn)) prn_no);
            Bit#(TAdd#(TLog#(max_prn),10)) temp_address = zeroExtend(prn_no)<<10;//(prn_no*1024
            rg_start_address <= temp_address;
            rg_stop_address <= temp_address + 1022;
            Bit#(TAdd#(TLog#(max_prn),10)) temp_prompt_address = temp_address + zeroExtend(code_shift_prn);
            rg_prn_address_l <= temp_prompt_address;
            rg_sample_count_p <= code_shift_sample;
            prn_val.put(False,temp_prompt_address,?);
            prn_val_sample.put(False,code_shift_prn,?);
            rg_IE <= 0;rg_QE <= 0;
                rg_IP <= 0;rg_QP <= 0;
                    rg_IL <= 0;rg_QL <= 0;
            rg_accumulate_count <= 0;
            rg_prompt_set <= 0;
            rg_readFlag <= 1;
        endmethod
        method Int#(magnitude_length) result_ie if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_IE;
        endmethod
        method Int#(magnitude_length) result_qe if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_QE;
        endmethod
        method Int#(magnitude_length) result_ip if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_IP;
        endmethod
        method Int#(magnitude_length) result_qp if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_QP;
        endmethod
        method Int#(magnitude_length) result_il if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_IL;
        endmethod
        method Int#(magnitude_length) result_ql if(rg_accumulate_count == fromInteger(valueOf(samples)));
            return rg_QL;
        endmethod
        method Action code_error_correction(Bit#(10) code_shift_prn,Bit#(TLog#(max_spcc)) code_shift_sample, Int#(TAdd#(TLog#(max_spcc),1)) code_error, Bit#(TLog#(max_prn)) prn_no);
            Bit#(TAdd#(TLog#(max_prn),10)) temp_address = zeroExtend(prn_no) << 1024;
            rg_start_address <= temp_address;
            rg_stop_address <= temp_address + 1022;
            rg_prn_address_l <= temp_address + zeroExtend(code_shift_prn);
            rg_sample_count_p <= code_shift_sample;
            rg_code_error <= code_error;
            prn_val_sample.put(False,code_shift_prn,?);
           rg_readFlag <= 3;
        endmethod
        method Bit#(10) corrected_code_prn if(rg_readFlag == 5);
            return rg_corrected_code_prn;
        endmethod
        method Bit#(TLog#(max_spcc)) corrected_code_sample if(rg_readFlag == 5);
            return rg_corrected_code_sample;
        endmethod
    endmodule : mkelp_correlator
endpackage : elp_correlator