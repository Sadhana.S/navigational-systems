import trackingfsm::*;
import BRAMCore::*;
import FixedPoint::*;
`include "defined_parameters.bsv"

module mkTestbench();
    Reg#(Bit#(2)) rg_Flag <- mkReg(0);
    Reg#(Int#(`Angle_length)) rg_carr_freq <- mkReg(0);
    Reg#(Int#(`Angle_length)) rg_carr_phase <- mkReg(0);
    Reg#(Bit#(10)) rg_code_shift_prn <- mkReg(0);
    Reg#(Bit#(TLog#(TAdd#(`Max_spcc,1)))) rg_code_shift_sample <- mkReg(0);
    Reg#(Int#(`Magnitude_length)) rg_ip <- mkReg(0);
    Reg#(Int#(`Magnitude_length)) rg_qp <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_change <- mkReg(0);
    Reg#(Bit#(TLog#(`Data_processed))) rg_count <- mkReg(0);
    Reg#(Bit#(1)) rg_control_bit <- mkReg(0);
    Reg#(Bit#(TLog#(`Max_PRN))) rg_prn_no <- mkReg(31);

    Integer prn = valueOf(`Max_PRN)*1024;
    Integer samp = valueOf(`Data_processed)*valueOf(`Samples);

    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
/*
    //For sampling frequency 12MHz
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_val_12",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_12",False); 
    FixedPoint#(TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) samp_freq = 357.913941333; 
*/    
    
    //For sampling frequency 16367600 Hz
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_value_16",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_16",False); 
    FixedPoint#(TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) samp_freq = 262.406662919; 

    Int#(2) kp1 = 1;
    Int#(2) kp2 = 0;
    Int#(16) kf1 = 999;
    Int#(16) kf2 = 22773;
    

    Ifc_trackingfsm#(`Samples,`Input_length,`Frequency_component_length,`Cordic_iterations,`Cordic_additional_scalling,`Angle_length,`Magnitude_length,`Max_PRN,`Data_processed,TAdd#(`Max_spcc,1),TMax#(`Common_hspcc,`Different_hspcc),`Even_spcc,`Odd_spcc,`Cordic_additional_scalling_arctan,`Cordic_iterations_arctan,`Cordic_iterations_division) trackingfsm <- mktrackingfsm(i_val,q_val,prn_val,prn_val_sample,kp1,kp2,kf1,kf2);

    rule r1(rg_Flag == 0);
        $display($time,"\n Prn no : %d ",rg_prn_no+1);
        $display($time,"\n Count = 2 \n");
        trackingfsm.get_inputs(594,6,1084844249,0,rg_control_bit,rg_prn_no,2);
        rg_Flag <= 1;
    endrule
    rule r2(rg_Flag == 1);
        Int#(`Angle_length) carr_freq = trackingfsm.carr_freq_out;
        rg_carr_freq <= carr_freq;
        Int#(`Angle_length) carr_phase = trackingfsm.carr_phase_out;
        rg_carr_phase <= carr_phase;
        Bit#(10) code_shift_prn = trackingfsm.code_shift_prn_out;
        rg_code_shift_prn <= code_shift_prn;
        Bit#(TLog#(TAdd#(`Max_spcc,1))) code_shift_sample = trackingfsm.code_shift_sample_out;
        rg_code_shift_sample <= code_shift_sample;
        Int#(`Magnitude_length) ip = trackingfsm.prompt_inphase;
        rg_ip <= ip;
        Int#(`Magnitude_length) qp = trackingfsm.prompt_quadphase;
        rg_qp <= qp;
        Bit#(1) sign = trackingfsm.sign_change;
        rg_sign_change <= sign;
        $display(" carrier frequency = %d \n carrier phase = %d \n code shift_prn = %d \t code_shift_sample = %d \n ip = %d \t qp = %d \n sign change = %d \n",carr_freq,carr_phase,code_shift_prn,code_shift_sample,ip,qp,sign);
        rg_Flag <= 2;
    endrule
    rule r3(rg_Flag == 2);
        Bit#(1) ctrl = ~rg_control_bit;
        rg_control_bit <= ctrl;
        $display($time,"\n Count = %d \n",rg_count+3);
        trackingfsm.get_inputs(rg_code_shift_prn,rg_code_shift_sample,rg_carr_freq,rg_carr_phase,ctrl,rg_prn_no,rg_count + 3);
        rg_Flag <= 1;
        rg_count <= rg_count + 1;
        if(rg_count == 19)
            $finish;
    endrule
endmodule : mkTestbench