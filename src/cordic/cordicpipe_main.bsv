package cordicpipe_main;

import FIFO::*;
import SpecialFIFOs ::*;
import Vector ::*;
import BRAMCore::*;

interface Ifc_cordicpipe_main#(numeric type angle_length,
                               numeric type samples,
                               numeric type cordic_iterations,
                               numeric type input_length,
                               numeric type frequency_comp_length,
                               numeric type cordic_additional_scalling,
                               numeric type ms_of_data_proceesed);
    method Action initialise(Int#(TAdd#(angle_length,1)) base_in, Int#(TAdd#(angle_length,1)) interval_in, Int#(angle_length) carrier_phase, Bit#(TLog#(ms_of_data_proceesed)) loop_cnt);
    method Action reinitialise;
    method Int#(TAdd#(input_length,frequency_comp_length)) i_out;
    method Int#(TAdd#(input_length,frequency_comp_length)) q_out;
    method Int#(angle_length) lastphase_out;
endinterface : Ifc_cordicpipe_main

module mkcordicpipe_main(BRAM_PORT#(Bit#(36),Int#(input_length)) i_val,
                         BRAM_PORT#(Bit#(36),Int#(input_length)) q_val,
                         Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_comp_length,cordic_additional_scalling,ms_of_data_proceesed) ifc_cordicpipe)
                         provisos(Add#(1,cordic_iterations,cordic_pipe_length),
                                  Add#(cordic_additional_scalling,TAdd#(input_length,frequency_comp_length),cordic_length_in_processing),
                                  Add#(cordic_additional_scalling,frequency_comp_length,cordic_scalling),
                                  Add#(TAdd#(frequency_comp_length,cordic_additional_scalling),input_length,cordic_length_in_processing),
                                  Add#(b,cordic_length_in_processing,TAdd#(TAdd#(input_length,1),TAdd#(cordic_scalling,2))),
                                  Add#(c,TLog#(samples),36),
                                  Add#(d,TLog#(ms_of_data_proceesed),36)
                                 );

    Bit#(TLog#(samples)) samp = fromInteger(valueOf(samples));
    Bit#(TLog#(cordic_pipe_length)) cordic_iter = fromInteger(valueOf(cordic_iterations));

    Vector#(31, Int#(angle_length)) atan_table;
        atan_table[0] = 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
        atan_table[1] = 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
        atan_table[2] = 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
        atan_table[3] = 'b00000101000100010001000111010100; // atan(2^-3)
        atan_table[4] = 'b00000010100010110000110101000011;
        atan_table[5] = 'b00000001010001011101011111100001;
        atan_table[6] = 'b00000000101000101111011000011110;
        atan_table[7] = 'b00000000010100010111110001010101;
        atan_table[8] = 'b00000000001010001011111001010011;
        atan_table[9] = 'b00000000000101000101111100101110;
        atan_table[10]= 'b00000000000010100010111110011000;
        atan_table[11]= 'b00000000000001010001011111001100;
        atan_table[12]= 'b00000000000000101000101111100110;
        atan_table[13]= 'b00000000000000010100010111110011;
        atan_table[14]= 'b00000000000000001010001011111001;
        atan_table[15]= 'b00000000000000000101000101111100;
        atan_table[16]= 'b00000000000000000010100010111110;
        atan_table[17]= 'b00000000000000000001010001011111;
        atan_table[18]= 'b00000000000000000000101000101111;
        atan_table[19]= 'b00000000000000000000010100010111;
        atan_table[20]= 'b00000000000000000000001010001011;
        atan_table[21]= 'b00000000000000000000000101000101;
        atan_table[22]= 'b00000000000000000000000010100010;
        atan_table[23]= 'b00000000000000000000000001010001;
        atan_table[24]= 'b00000000000000000000000000101000;
        atan_table[25]= 'b00000000000000000000000000010100;
        atan_table[26]= 'b00000000000000000000000000001010;
        atan_table[27]= 'b00000000000000000000000000000101;
        atan_table[28]= 'b00000000000000000000000000000010;
        atan_table[29]= 'b00000000000000000000000000000001;
        atan_table[30]= 'b00000000000000000000000000000000;

    FIFO#(Tuple3 #(Int#(cordic_length_in_processing), Int#(cordic_length_in_processing), Int#(TAdd#(angle_length,1)))) pre_fifo <- mkPipelineFIFO;  
    Vector#(cordic_pipe_length,FIFO#(Tuple3 #(Int#(cordic_length_in_processing),Int#(cordic_length_in_processing),Int#(angle_length)))) cordic_pipe <- replicateM(mkPipelineFIFO);

    Reg#(Int#(TAdd#(angle_length,1))) rg_current_angle <- mkReg(0);
    Reg#(Int#(TAdd#(angle_length,1))) rg_base_angle <- mkReg(0);
    Reg#(Int#(TAdd#(angle_length,1))) rg_doppler_interval <- mkReg(0); 
    Reg#(Int#(TAdd#(input_length,frequency_comp_length))) rg_i_out <- mkReg(0);
    Reg#(Int#(TAdd#(input_length,frequency_comp_length))) rg_q_out <- mkReg(0);
    Reg#(Bit#(TLog#(samples))) rg_output_count <- mkReg(0);
    Reg#(Bit#(36)) rg_input_address <- mkReg(0);
    Reg#(Bit#(36)) rg_start_address <- mkReg(0);
    Reg#(Bit#(36)) rg_stop_address <- mkReg(0);
    Reg#(Bit#(1)) rg_flag <- mkReg(0);
    Reg#(Bit#(1)) rg_cordic_start <- mkReg(0);

    (* descending_urgency = " r1_get_inputs_request_to_bram, r2_get_input_values_from_bram " *)
    rule r1_get_inputs_request_to_bram(rg_cordic_start==1);
        if(rg_input_address == rg_stop_address)
            rg_cordic_start <= 0;
        else
        begin
            i_val.put(False,rg_input_address,?);
            q_val.put(False,rg_input_address,?);
            Int#(TAdd#(angle_length,2)) temp = zeroExtend(rg_current_angle) + zeroExtend(rg_base_angle);
            if(temp >= 4294967295)
                rg_current_angle <= truncate(temp - 4294967296);
            else
                rg_current_angle <= truncate(temp);
            if(rg_input_address == rg_start_address)
            begin
                rg_flag <= 1;
                //$display($time,"Initial angle = %d  and angle = %d \n",rg_base_angle,rg_current_angle);
            end
            rg_input_address <= rg_input_address + 1;
        end
    endrule
    rule r2_get_input_values_from_bram(rg_flag == 1);//Values from i_val and q_val BRAMs
        if(rg_cordic_start == 0)
        begin
            rg_flag <= 0;
            pre_fifo.enq(tuple3(0,0,0));
        end
        else
        begin
            Int#(cordic_length_in_processing) i = signExtend(i_val.read) << valueOf(cordic_scalling);
            Int#(cordic_length_in_processing) q = signExtend(q_val.read) << valueOf(cordic_scalling);
            Int#(TAdd#(angle_length,1)) temp_angle = 4294967295 - rg_current_angle;
            //$display($time," i = %d, q = %d and angle = %d \n",i,q,rg_current_angle);
            pre_fifo.enq(tuple3(i,q,temp_angle));
        end
    endrule
    rule r3_converge_angle;//Converging all the angles within first quadrant for cordic operations
        match {.i, .q, .a2} = pre_fifo.first; pre_fifo.deq;
        Int#(cordic_length_in_processing) i4 = 0;
        Int#(cordic_length_in_processing) q4 = 0;
        Int#(angle_length) a3 = 0;
        if((a2 > 1073741824)&&(a2 <= 2147483648)) //second quadrant
        begin
            i4 = -q;
            q4 = i;
            a3 = truncate(a2 + 3221225472);
        end
        else if((a2 > 2147483648)&&(a2 <= 3221225472)) //third quadrant
                    begin
                        i4 = -i;
                        q4 = -q;
                        a3 = truncate(a2 + 2147483648);
                    end
                    else if((a2 > 3221225472)&&(a2<=4294967295)) //fourth quadrant
                        begin
                            i4 = q;
                            q4 = -i;
                            a3 = truncate(a2 + 1073741824);
                        end
                        else
                        begin //first quadrant
                            i4 = i;
                            q4 = q;
                            a3 = truncate(a2);
                        end
        cordic_pipe[0].enq(tuple3 (i4,q4,a3));
    endrule
    for(Bit#(TLog#(cordic_pipe_length)) j=0; j<cordic_iter; j=j+1)//Cordic iterations
    begin
        rule r4_cordic_processing;
            match {.x,.y,.z} = cordic_pipe[j].first; cordic_pipe[j].deq;
            let a2=x >> j;
            let a3=y >> j;
            cordic_pipe[j+1].enq(tuple3 (((z>0)?x-a3:x+a3),((z>0)?y+a2:y-a2),((z>0)?z-atan_table[j]:z+atan_table[j])));
        endrule
    end
    rule r5_truncate_and_pass_output;
        match{.i1,.q1,.angle1} = cordic_pipe[cordic_iter].first;cordic_pipe[cordic_iter].deq;
        rg_i_out <= truncate(i1>>valueOf(cordic_additional_scalling));
        rg_q_out <= truncate(q1>>valueOf(cordic_additional_scalling));
        //$display($time," %d : i_out = %d and q_out = %d \n", rg_output_count,rg_i_out,rg_q_out);
        rg_output_count <= rg_output_count + 1;
    endrule

    method Action initialise(Int#(TAdd#(angle_length,1)) base_in, Int#(TAdd#(angle_length,1)) interval_in, Int#(angle_length) carrier_phase, Bit#(TLog#(ms_of_data_proceesed)) loop_cnt);
        rg_base_angle <= base_in;
        rg_current_angle <= signExtend(carrier_phase);
        rg_doppler_interval <= interval_in;
        Bit#(36) temp_address = zeroExtend(loop_cnt-1)*fromInteger(valueOf(samples));
        rg_start_address <= temp_address;
        rg_stop_address <= temp_address + fromInteger(valueOf(samples));
        rg_input_address <= temp_address;
        rg_cordic_start <= 1;
        rg_output_count <= 0;
    endmethod
    method Action reinitialise;
        rg_output_count <= 0;
        rg_input_address <= 0;
        rg_cordic_start <= 1;
        rg_current_angle <= 0;
        rg_base_angle <= rg_base_angle + rg_doppler_interval;
    endmethod
    method Int#(TAdd#(input_length,frequency_comp_length)) i_out if((rg_output_count > 0)&&(rg_output_count <= fromInteger(valueOf(samples)+1)));
        return rg_i_out;
    endmethod
    method Int#(TAdd#(input_length,frequency_comp_length)) q_out if((rg_output_count > 0)&&(rg_output_count <= fromInteger(valueOf(samples)+1)));
        return rg_q_out;
    endmethod
    method Int#(angle_length) lastphase_out if(rg_output_count == fromInteger(valueOf(samples)+1));
        return truncate(rg_current_angle);
    endmethod
endmodule : mkcordicpipe_main
    
endpackage : cordicpipe_main