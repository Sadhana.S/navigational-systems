/*
Module: Acquisition FSM
Description: This module implements the Acquisition FSM. The state machine is as follows:
	1. Generate scaled versions of the frequencies which are required for freq. shifting.
	   In the first cycle, the first input (-10kHz) is sent to the freq synthesizer.
	2. In the second cycle, the base angle and the interval angle are read from the freq
	   synthesizer, and are sent to the freq shift block. This block would then read the sample data
	   and perform freq. shifting on each sample.
	3. Third cycle onwards, for the next `samp_freq number of cycles, the outputs of the freq shift
	   block are read, and are fed to the code shift module, which in turn does the correlation.
	4. Once correlation is done for this particular freq shift, the freq shift module is again
	   called to perform freq shift on the sample data for the next freq shift.
	5. The next cycle onwards the new freq shifted data is sent to the code shift block.
	6. This happens untill all freq. shift have been performed.
    7. ?

The code is written for 32 PRN values (32 satellite) and the ouput from RF is in Intermediate Frequency(IF)
*/

/*
-> There is multiplication involved in frequency conversion which is to be given to tracking
*/

package acquisitionfsm;

    import freqsynthesiser ::*;//Provides scalled version of the base frequency(IF - 10K) and interval frequency
    import cordicpipe_main::*;
    import cordicpipe_accumulation::*;
    import cordicpipe_finefrequencyshift::*;
    import codecorrelator::*;//Performs circular correlation of the frequenct shifted values with respective PRN
    import magnitudepipe::*;
    import Vector::*;
    import FixedPoint::*;
    import BRAMCore::*;

    interface Ifc_acquisitionfsm#(numeric type doppler_interval,
                                  numeric type fine_doppler_interval,
                                  numeric type start_frequency,
                                  numeric type fine_start_frequency,
                                  numeric type angle_length,
                                  numeric type sampling_frequency,
                                numeric type accumulated_length,
                                numeric type samples,
                                numeric type frequency_shift_iterations,
                                numeric type fine_frequency_shift_iterations,
                                numeric type cordic_iterations,
                                numeric type input_length,
                                numeric type frequency_comp_length,
                                numeric type common_hspcc,
                                numeric type different_hspcc,
                                numeric type cordic_additional_scalling,
                                numeric type max_SPCC,
                                numeric type max_PRN,
                                numeric type magnitude_cordic_iterations,
                                numeric type magnitude_scalling_in_processing,
                                numeric type magnitude_length,
                                numeric type samples_correlated,
                                numeric type systolic_cells,
                                numeric type systolic_iterations,
                                numeric type odd_spcc,
                                numeric type even_spcc,
                                numeric type ms_of_data_processed);
        method Action initialise;
        method Action prn_value(Bit#(TLog#(max_PRN)) prn_no);
        method Bool acquisition_status;
        method Bit#(TLog#(samples_correlated)) code_phase_out;
        method Bit#(TLog#(frequency_shift_iterations)) frequency_error;
        method Bit#(TLog#(fine_frequency_shift_iterations)) fine_frequency_error;
    endinterface : Ifc_acquisitionfsm

    module mkacquisitionfsm(BRAM_PORT#(Bit#(36),Int#(input_length)) i_val,
                            BRAM_PORT#(Bit#(36),Int#(input_length)) q_val,
                            BRAM_PORT#(Bit#(TAdd#(TLog#(max_PRN),10)),Bit#(1)) prn_val,
                            BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                            FixedPoint#(TAdd#(TSub#(angle_length,TLog#(sampling_frequency)),2),20) samp_freq,
                            Ifc_acquisitionfsm#(doppler_interval,fine_doppler_interval,start_frequency,fine_start_frequency,
                                                angle_length,sampling_frequency,accumulated_length,samples,frequency_shift_iterations,
                                                fine_frequency_shift_iterations,cordic_iterations,input_length,frequency_comp_length,
                                                common_hspcc,different_hspcc,cordic_additional_scalling,max_SPCC,max_PRN,magnitude_cordic_iterations,
                                                magnitude_scalling_in_processing,magnitude_length,samples_correlated,systolic_cells,systolic_iterations,odd_spcc,even_spcc, ms_of_data_processed) ifc_acquisition_fsm)
                                                provisos(Add#(TLog#(max_SPCC),TAdd#(input_length,frequency_comp_length),accumulated_length),
                                                         Add#(accumulated_length,TLog#(samples_correlated),magnitude_length),
                                                         Add#(x,TLog#(samples),36),
                                                         Add#(1,y,TLog#(sampling_frequency)),
                                                         Add#(w,TAdd#(TLog#(start_frequency),1),y),
                                                         Add#(z,TAdd#(TLog#(doppler_interval),1),y),
                                                         Add#(g,TLog#(frequency_shift_iterations), TAdd#(TLog#(start_frequency),1)),//Should be removed after removing freqsynthesiser module
                                                         Add#(h,TAdd#(input_length,frequency_comp_length),magnitude_length),
                                                         Add#(k,TLog#(ms_of_data_processed),36)
                                                         );
        Bit#(TAdd#(TLog#(max_PRN),1)) prn_ref = fromInteger(valueOf(max_PRN));
        Bit#(TLog#(frequency_shift_iterations)) freq_shift_count_ref = fromInteger(valueOf(frequency_shift_iterations));
        Int#(TAdd#(TLog#(start_frequency),1)) fine_start_freq = fromInteger(valueOf(fine_start_frequency));
        Int#(TAdd#(TLog#(start_frequency),1)) start_freq = fromInteger(valueOf(start_frequency));
        Bit#(TLog#(fine_frequency_shift_iterations)) fine_freq_shift_count_ref = fromInteger(valueOf(fine_frequency_shift_iterations));
        Int#(TAdd#(TLog#(doppler_interval),1)) doppler_inter = fromInteger(valueOf(doppler_interval));
        Int#(TAdd#(TLog#(doppler_interval),1)) fine_doppler_inter = fromInteger(valueOf(fine_doppler_interval));

        Reg#(Bit#(4)) rg_flag <- mkReg(0);
        Reg#(Bit#(TLog#(frequency_shift_iterations))) rg_freq_shift_count <- mkReg(0);
        Reg#(Bit#(TLog#(fine_frequency_shift_iterations))) rg_fine_freq_shift_count <- mkReg(0);
        Reg#(Bit#(TAdd#(TLog#(max_PRN),1))) rg_prn_count <- mkReg(0);
        Reg#(Bit#(TLog#(max_PRN))) rg_prn <- mkReg(0);


        Vector#(max_PRN,Reg#(Bool)) acq_state <- replicateM(mkReg(False));
        Vector#(max_PRN,Reg#(Bit#(TLog#(frequency_shift_iterations)))) frequency_shift <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(TLog#(fine_frequency_shift_iterations)))) fine_freq_shift <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(TLog#(samples_correlated)))) firstmax_code <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(TLog#(samples_correlated)))) secondmax_code <- replicateM(mkReg(0));
        /*Vector#(max_PRN,Reg#(Bit#(10))) firstmax_code_prn <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(TLog#(max_SPCC)))) firstmax_code_sample <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(10))) secondmax_code_prn <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Bit#(TLog#(max_SPCC)))) secondmax_code_sample <- replicateM(mkReg(0));*/
        Vector#(max_PRN,Reg#(Int#(magnitude_length))) firstmax <- replicateM(mkReg(0));
        Vector#(max_PRN,Reg#(Int#(magnitude_length))) secondmax <- replicateM(mkReg(0));

        Reg#(Int#(magnitude_length)) rg_magnitude_compare <- mkReg(0);

        Ifc_freqsynthesiser#(TAdd#(TLog#(doppler_interval),1),TAdd#(TLog#(start_frequency),1),TAdd#(angle_length,1),TAdd#(TSub#(angle_length,TLog#(sampling_frequency)),2),20) freqsynthesiser <- mkfreqsynthesiser;
        Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_comp_length,cordic_additional_scalling,ms_of_data_processed) cordic_main <- mkcordicpipe_main(i_val,q_val);
        Ifc_cordicpipe_accumulation#(input_length,frequency_comp_length,samples_correlated,accumulated_length,max_SPCC,common_hspcc,different_hspcc) cordic_accumulation <- mkcordicpipe_accumulation(prn_val_sample,cordic_main);
        Ifc_magnitudepipe#(magnitude_cordic_iterations,magnitude_scalling_in_processing,magnitude_length) magnitudepipe <- mkmagnitudepipe;
        Ifc_codecorrelator#(angle_length,accumulated_length,samples,cordic_iterations,input_length,frequency_comp_length,common_hspcc,different_hspcc,cordic_additional_scalling,max_SPCC,max_PRN,magnitude_cordic_iterations,magnitude_scalling_in_processing,magnitude_length,samples_correlated,systolic_cells,systolic_iterations) codecorrelator <- mkcodecorrelator(prn_val,cordic_accumulation,magnitudepipe);
        Ifc_cordicpipe_finefrequencyshift#(samples,input_length,frequency_comp_length,magnitude_length,accumulated_length,max_PRN,max_SPCC,odd_spcc,even_spcc) finefreq <- mkcordicpipe_finefrequencyshift(prn_val,prn_val_sample,cordic_main);
        //Ifc_fine_code_shift#() fine_codeshift <- mkfine_code_shift(prn_val,prn_val_sample,cordic_main);        

        // Inputs to frequency synthesiser such as scalled sampling frequency, intermediate frequency and doppler interval
        rule r1_inputs_to_freqsynthesiser(rg_flag == 1);
            freqsynthesiser.get_inputs(doppler_inter,samp_freq,start_freq);
            rg_flag <= 2;
        endrule
        // Outputs of frequency synthesiser is given to cordicpipe via code correlator
        rule r2_outputs_from_freqsynthesiser_and_initialise_cordicpipe(rg_flag == 2);
            Int#(TAdd#(angle_length,1)) base_angle = freqsynthesiser.results_base;
            Int#(TAdd#(angle_length,1)) interval_angle = freqsynthesiser.results_interval;
            cordic_main.initialise(base_angle,interval_angle,0,1);
            cordic_accumulation.initialise;
            //$display($time,"Initialised cordic \n");
            rg_flag <= 3;
        endrule
        // Initialising code shift cluorrelator - rg_prn_count will seek the starting address of the PRN value 
        rule r3_initialising_codecorrelator((rg_prn_count < prn_ref)&&(rg_flag == 3));
            codecorrelator.initialise(truncate(rg_prn_count));
            //$display($time,"Intialised code correlator \n");
            rg_flag <= 4;
        endrule
        // Outputs from code correlator are obtained and updating the values based on the first maximum
        rule r4_outputs_from_code_correlator(rg_flag == 4);
            Int#(magnitude_length) a = codecorrelator.result_firstmax();
            Int#(magnitude_length) b = codecorrelator.result_secondmax();
            Bit#(TLog#(samples_correlated)) c = codecorrelator.result_firstmax_code();
            Bit#(TLog#(samples_correlated)) d = codecorrelator.result_secondmax_code();
            if(a > firstmax[rg_prn_count])
            begin
                frequency_shift[rg_prn_count] <= rg_freq_shift_count;//complete it
                firstmax[rg_prn_count] <= a;
                secondmax[rg_prn_count] <= b;
                firstmax_code[rg_prn_count] <= c;
                secondmax_code[rg_prn_count] <= d;
                /*firstmax_code_prn[rg_prn_count] <= c[10:1];
                secondmax_code_prn[rg_prn_count] <= d[10:1];
                if(c[0] == 0)
                    firstmax_code_sample[rg_prn_count] <= 0;
                else
                    firstmax_code_sample[rg_prn_count] <= fromInteger(valueOf(common_hspcc));
                if(d[0] == 0)
                    secondmax_code_sample[rg_prn_count] <= 0;
                else
                    secondmax_code_sample[rg_prn_count] <= fromInteger(valueOf(common_hspcc));*/
            end
            $display($time," %d %d First_max = %d corresponding code phase = %d \t Second_max = %d corresponding code phase = %d \n",rg_freq_shift_count,rg_prn_count,a,c,b,d);
            rg_flag <= 3;
            rg_prn_count <= rg_prn_count + 1;
        endrule
        // Reinitialising cordic pipe for next frequency shift as well as reinitialisation of PRN value and incrementing the frequency bin index
        rule r5_reinitialise_cordipipe((rg_prn_count == prn_ref)&&(rg_freq_shift_count < freq_shift_count_ref));
            if(rg_freq_shift_count == freq_shift_count_ref - 1)
                rg_flag <= 5;
            else
            begin
                cordic_main.reinitialise;
                cordic_accumulation.initialise;
                rg_flag <= 3;
            end
            rg_prn_count <= 0;
            rg_freq_shift_count <= rg_freq_shift_count + 1;
        endrule
        //After completion of code shifts for all frequency shifts, acquisition ratio is calculated to say whether a satellite is acquired
        //Second_maximum is multiplied by the acquisition threshold and compared with first maximum. If first maximum is greater, then the satellite is acquired
        rule r6_acquisition_ratio_computation(rg_flag == 5);
            FixedPoint#(magnitude_length,8) temp1 = fromInt(secondmax[rg_prn_count]);
            FixedPoint#(2,8) temp2 = 1.38;//sqrt(Required acquisition threshold)
            Int#(TAdd#(magnitude_length,2)) temp3 = fxptGetInt(fxptMult(temp1,temp2));
            Int#(TAdd#(magnitude_length,2)) temp4 = zeroExtend(firstmax[rg_prn_count]);
            if(temp4 > temp3)
            begin
                acq_state[rg_prn_count] <= True;
                $display($time,"  => Satellite %d is acquired(%d,%d) with code phase = %d and freq_bin = %d ",rg_prn_count+1,firstmax[rg_prn_count],secondmax[rg_prn_count],firstmax_code[rg_prn_count],frequency_shift[rg_prn_count]);
            end
            //else
              //  $display($time," Satellite %d is not acquired(%d,%d) with code phase = %d and freq_bin = %d ",rg_prn_count+1,firstmax[rg_prn_count],secondmax[rg_prn_count],firstmax_code[rg_prn_count],frequency_shift[rg_prn_count]);
            if(rg_prn_count == prn_ref-1)
                rg_flag <= 6;
            else
                rg_prn_count <= rg_prn_count + 1;
        endrule
       /* rule r7_fine_code_shift_frequency_compute(rg_flag == 6);
            if(acq_state[rg_prn_count] == True)
            begin
                Int#(TAdd#(TLog#(start_frequency),1)) freq_interval = unpack(zeroExtend(frequency_shift[rg_prn_count]));
                Int#(TAdd#(TLog#(start_frequency),1)) temp6 = temp5*500;
                Int#(TAdd#(TLog#(start_frequency),1)) temp7 = start_freq + temp6;
                freqsynthesiser.get_inputs(fine_doppler_inter,samp_freq,temp7);
                rg_flag <= 7;
            end
            else
                rg_flag <= 11;
        endrule
        rule r8_fine_code_shift_initialisation(rg_flag == 7);
            Bit#(10) prompt_prn;
            Bit#(TLog#(max_SPCC)) stop_sample;
            if(firstmax_code_sample[rg_prn_count] == 0)
                stop_sample = fromInteger(valueOf(common_hspcc));
            else
                stop_sample = fromInteger(valueOf(TMul#(common_hspcc,2)));
            fine_codeshift.initialise(prompt_prn,stop_sample,truncate(rg_prn_count));
            rg_flag <= 8;
        endrule
        rule r9_corrected_code_shift(rg_flag == 8);
            
        endrule
        */
        rule r7_frequency_conversion(rg_flag == 6);
            if(acq_state[rg_prn_count] == True)
            begin
                Int#(TAdd#(TLog#(start_frequency),1)) temp5 = unpack(zeroExtend(frequency_shift[rg_prn_count]));
                Int#(TAdd#(TLog#(start_frequency),1)) temp6 = temp5*500;
                //$display($time,"prn_count = %d : %d \n",rg_prn_count,temp6);
                Int#(TAdd#(TLog#(start_frequency),1)) temp7 = fine_start_freq + temp6;
                freqsynthesiser.get_inputs(fine_doppler_inter,samp_freq,temp7);//for sampling frequency 12 MHz
                rg_flag <= 7;
            end
            else 
                rg_flag <= 11;
        endrule
        rule r8_fine_frequency_synthesiser(rg_flag == 7);
            Int#(TAdd#(angle_length,1)) base_angle = freqsynthesiser.results_base;
            Int#(TAdd#(angle_length,1)) interval_angle = freqsynthesiser.results_interval;
            cordic_main.initialise(base_angle,interval_angle,0,1);
            if(firstmax_code[rg_prn_count][0] == 0)
                finefreq.initialise(firstmax_code[rg_prn_count][10:1],0,truncate(rg_prn_count));
            else
                finefreq.initialise(firstmax_code[rg_prn_count][10:1],fromInteger(valueOf(common_hspcc)),truncate(rg_prn_count));
            rg_fine_freq_shift_count <= 0;
            rg_flag <= 8;
        endrule
        rule r9_output_from_fine_frequency_shift((rg_flag == 8)&&(rg_fine_freq_shift_count < fine_freq_shift_count_ref));
            Int#(TAdd#(accumulated_length,TLog#(samples_correlated))) i = finefreq.inphase_out;
            Int#(TAdd#(accumulated_length,TLog#(samples_correlated))) q = finefreq.quadphase_out;
            //$display($time,"prn_shift = %d and freq_count = %d : Inphase = %d and Quadphase = %d",rg_prn_count,rg_fine_freq_shift_count,i,q);
            magnitudepipe.get_inputs(i,q);
            rg_flag <= 9;
        endrule
        rule r10_magnitude_computation(rg_flag == 9);
            Int#(magnitude_length) mag_out <- magnitudepipe.result_magnitude;
            //$display($time,"magnitude : %d magnitude_compare : %d  \n",mag_out,rg_magnitude_compare);
            if(mag_out > rg_magnitude_compare)
            begin
                fine_freq_shift[rg_prn_count] <= rg_fine_freq_shift_count;
                rg_magnitude_compare <= mag_out;
            end
            rg_fine_freq_shift_count <= rg_fine_freq_shift_count + 1;
            rg_flag <= 10; 
        endrule
        rule r11_fine_frequency_reintialisation(rg_flag == 10);
            if(rg_fine_freq_shift_count == fine_freq_shift_count_ref)
                rg_flag <= 11;
            else
            begin
                cordic_main.reinitialise;
                if(firstmax_code[rg_prn_count][0] == 0)
                    finefreq.initialise(firstmax_code[rg_prn_count][10:1],0,truncate(rg_prn_count));
                else
                    finefreq.initialise(firstmax_code[rg_prn_count][10:1],fromInteger(valueOf(common_hspcc)),truncate(rg_prn_count));
                rg_flag <= 8;
            end
        endrule
        rule r11_prn_update(rg_flag == 11);
            if(rg_prn_count == 0)
                rg_flag <= 12;
            else
            begin
                rg_prn_count <= rg_prn_count - 1;
                rg_magnitude_compare <= 0;
                rg_flag <= 6;
            end
        endrule

        method Action initialise;
            rg_flag <= 1;
        endmethod
        method Action prn_value(Bit#(TLog#(max_PRN)) prn_no) if(rg_flag == 12);
            rg_prn <= prn_no;
        endmethod
        method Bool acquisition_status;
            return acq_state[rg_prn];
        endmethod
        method Bit#(TLog#(samples_correlated)) code_phase_out;
            return firstmax_code[rg_prn];
        endmethod
        method Bit#(TLog#(frequency_shift_iterations)) frequency_error;
            return frequency_shift[rg_prn];
        endmethod
        method Bit#(TLog#(fine_frequency_shift_iterations)) fine_frequency_error;
            return fine_freq_shift[rg_prn];
        endmethod
    endmodule : mkacquisitionfsm

endpackage : acquisitionfsm
