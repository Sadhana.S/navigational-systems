/*To put proper reinitialise*/ 

package cordicpipe_finefrequencyshift;

    import BRAMCore::*;
    import cordicpipe_main::*;

    interface Ifc_cordicpipe_finefrequencyshift#(numeric type samples,
                                                 numeric type input_length,
                                                 numeric type frequency_component_length,
                                                 numeric type magnitude_length,
                                                 numeric type accumulated_length,
                                                 numeric type max_prn,
                                                 numeric type max_spcc,
                                                 numeric type odd_spcc,
                                                 numeric type even_spcc
                                                );
        method Action initialise(Bit#(10) prn_number, Bit#(TLog#(max_spcc)) prn_sample, Bit#(TLog#(max_prn)) satellite_number);
        //method Action reinitialise();
        method Int#(magnitude_length) inphase_out;
        method Int#(magnitude_length) quadphase_out;
    endinterface : Ifc_cordicpipe_finefrequencyshift

    module mkcordicpipe_finefrequencyshift(BRAM_PORT#(Bit#(TAdd#(TLog#(max_prn),10)),Bit#(1)) prn_val,
                                           BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample,
                                           Ifc_cordicpipe_main#(angle_length,samples,cordic_iterations,input_length,frequency_component_length,cordic_additional_scalling,ms_of_data_processed) ifc_cordic_main,
                                           Ifc_cordicpipe_finefrequencyshift#(samples,input_length,frequency_component_length,magnitude_length,
                                                                              accumulated_length,max_prn,max_spcc,odd_spcc,even_spcc) ifc_finefreq)
                                           provisos(Add#(a,TAdd#(input_length,frequency_component_length),magnitude_length));
    
        Reg#(Bit#(TAdd#(TLog#(max_prn),10))) rg_prn_start_address <- mkReg(0);
        Reg#(Bit#(TAdd#(TLog#(max_prn),10))) rg_current_prn_address <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_prn_sample_reference <- mkReg(0);
        Reg#(Bit#(TLog#(max_spcc))) rg_prn_current_sample <- mkReg(0);
        Reg#(Bit#(1)) rg_current_prn <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_accumulate_I <- mkReg(0);
        Reg#(Int#(magnitude_length)) rg_accumulate_Q <- mkReg(0);
        Reg#(Bit#(TLog#(samples))) rg_accumulate_count <- mkReg(fromInteger(valueOf(samples)));

        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
        
        (* descending_urgency = "r1_inital_prn_fetch, r2_correlation" *)
        rule r1_inital_prn_fetch(rg_accumulate_count == 0);
            rg_current_prn <= prn_val.read;
            Bit#(1) temp_prn_ref = prn_val_sample.read;
            if(temp_prn_ref == 1)
                rg_prn_sample_reference <= fromInteger(valueOf(odd_spcc)-1);
            else
                rg_prn_sample_reference <= fromInteger(valueOf(even_spcc)-1);
            if(((temp_prn_ref == 1)&&(rg_prn_current_sample == fromInteger(valueOf(odd_spcc))))||((temp_prn_ref == 0)&&(rg_prn_current_sample == fromInteger(valueOf(even_spcc)))))
            begin
            if(rg_current_prn_address[9:0] == 1022)
            begin
                Bit#(TAdd#(TLog#(max_prn),10))temp_prn_address = rg_current_prn_address - 1022;
                rg_current_prn_address <= temp_prn_address;
                prn_val.put(False,temp_prn_address,?);
                prn_val_sample.put(False,0,?);
            end
            else
            begin
                Bit#(TAdd#(TLog#(max_prn),10))temp_prn_address = rg_current_prn_address + 1;
                rg_current_prn_address <= temp_prn_address;
                prn_val.put(False,temp_prn_address,?);
                prn_val_sample.put(False,temp_prn_address[9:0],?);
            end
            end
            rg_accumulate_count <= 1;
            rg_readFlag <= 1;
        endrule
        rule r2_correlation(rg_readFlag == 1);
            let i = ifc_cordic_main.i_out;
            let q = ifc_cordic_main.q_out;
            if(rg_current_prn == 1)
            begin
                rg_accumulate_I <= rg_accumulate_I + signExtend(i);
                rg_accumulate_Q <= rg_accumulate_Q + signExtend(q);
            end
            else
            begin
                rg_accumulate_I <= rg_accumulate_I - signExtend(i);
                rg_accumulate_Q <= rg_accumulate_Q - signExtend(q);
            end
            rg_accumulate_count <= rg_accumulate_count + 1;
            if(rg_prn_current_sample == rg_prn_sample_reference)
            begin
                rg_prn_current_sample <= 0;
                
                Bit#(1) temp_prn_ref = prn_val_sample.read;
                if(temp_prn_ref == 1)
                    rg_prn_sample_reference <= fromInteger(valueOf(odd_spcc)-1);
                else
                    rg_prn_sample_reference <= fromInteger(valueOf(even_spcc)-1);
                rg_current_prn <= prn_val.read;

                if(rg_current_prn_address[9:0] == 1022)
                begin
                    rg_current_prn_address <= 0;
                    prn_val.put(False,0,?);
                    prn_val_sample.put(False,0,?);
                end
                else
                begin
                    Bit#(TAdd#(TLog#(max_prn),10))temp_prn_address = rg_current_prn_address + 1;
                    rg_current_prn_address <= temp_prn_address;
                    prn_val.put(False,temp_prn_address,?);
                    prn_val_sample.put(False,temp_prn_address[9:0],?);
                end
            end
            else
                rg_prn_current_sample <= rg_prn_current_sample + 1;
        endrule

        method Action initialise(Bit#(10) prn_number, Bit#(TLog#(max_spcc)) prn_sample, Bit#(TLog#(max_prn)) satellite_number);
            Bit#(TAdd#(TLog#(max_prn),10)) temp_address = zeroExtend(satellite_number)*1024;
            Bit#(TAdd#(TLog#(max_prn),10)) temp_prn_address = temp_address + zeroExtend(prn_number);
            rg_current_prn_address <= temp_prn_address;
            rg_prn_start_address <= temp_address;
            rg_prn_current_sample <= prn_sample;
            prn_val.put(False,temp_prn_address,?);
            prn_val_sample.put(False,prn_number,?);
            rg_accumulate_count <= 0;
            rg_accumulate_I <= 0;
            rg_accumulate_Q <= 0;
            rg_readFlag <= 0;
        endmethod
        /*method Action reinitialise();
        endmethod*/
        method Int#(magnitude_length) inphase_out if(rg_accumulate_count == fromInteger(valueOf(samples)+1));
            return rg_accumulate_I;
        endmethod
        method Int#(magnitude_length) quadphase_out if(rg_accumulate_count == fromInteger(valueOf(samples)+1)); 
            return rg_accumulate_Q;
        endmethod  
    endmodule : mkcordicpipe_finefrequencyshift
    
endpackage : cordicpipe_finefrequencyshift