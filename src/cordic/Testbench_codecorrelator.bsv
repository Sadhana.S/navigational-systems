import codecorrelator::*;
import magnitudepipe::*;
import cordicpipe_accumulation::*;
import cordicpipe_main::*;
import freqsynthesiser::*;
import BRAMCore::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Reg#(Bit#(2)) rg_flag <- mkReg(0);

    Integer samp1 = valueOf(`Samples);
    Integer prn = valueOf(`Max_PRN)*1024;
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_value_16",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_16",False);  

    Ifc_freqsynthesiser#(TAdd#(TLog#(`Doppler_interval),1),TAdd#(TLog#(`Start_frequency),1),TAdd#(`Angle_length,1),TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) freqsynthesiser <- mkfreqsynthesiser;
    Ifc_cordicpipe_main#(`Angle_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Cordic_additional_scalling,`Data_processed) cordic_main <- mkcordicpipe_main(i_val,q_val);
    Ifc_cordicpipe_accumulation#(`Input_length,`Frequency_component_length,`Samples_correlated,`Accumulated_length,TAdd#(`Max_spcc,1),`Common_hspcc,`Different_hspcc) cordic_accumulation <- mkcordicpipe_accumulation(prn_val_sample,cordic_main);

    Ifc_magnitudepipe#(`Magnitude_cordic_iterations,`Magnitude_scalling_in_processing,`Magnitude_length) magnitudepipe <- mkmagnitudepipe;
    Ifc_codecorrelator#(`Angle_length,`Accumulated_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Common_hspcc,`Different_hspcc,`Cordic_additional_scalling,TAdd#(`Max_spcc,1),`Max_PRN,`Magnitude_cordic_iterations,`Magnitude_scalling_in_processing,`Magnitude_length,`Samples_correlated,`Systolic_cells,`Systolic_iterations) codecorr <- mkcodecorrelator(prn_val,cordic_accumulation,magnitudepipe);

    rule r1(rg_flag == 0);
        freqsynthesiser.get_inputs(`Doppler_interval,262.406662919,`Start_frequency);//for sampling frequency 16367600 MHz
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        Int#(TAdd#(`Angle_length,1)) base_angle = freqsynthesiser.results_base;
        Int#(TAdd#(`Angle_length,1)) interval_angle = freqsynthesiser.results_interval;
        $display($time,"Base angle  = %d and Interval angle = %d \n",base_angle,interval_angle);
        cordic_main.initialise(base_angle,interval_angle,0,0);
        cordic_accumulation.initialise;
        rg_flag <= 2;
    endrule
    rule r3(rg_flag == 2);
        codecorr.initialise(0);
        rg_flag <= 3;
    endrule
    rule r4(rg_flag == 3);
        Int#(`Magnitude_length) first_max = codecorr.result_firstmax();
        Int#(`Magnitude_length) second_max = codecorr.result_secondmax();
        Bit#(TLog#(`Samples_correlated)) first_code = codecorr.result_firstmax_code();
        Bit#(TLog#(`Samples_correlated)) second_code = codecorr.result_secondmax_code();
        $display($time," First Max = %d corresponding code = %d \n Second Max = %d corresponding code = %d \n",first_max,first_code,second_max,second_code);
        $finish;
    endrule
    
endmodule : mkTestbench