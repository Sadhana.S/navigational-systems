import freqdiscriminator::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Reg#(Bit#(1)) rg_flag <- mkReg(0);

    Ifc_freqdiscriminator#(`Angle_length,`Samples) freqdisc <- mkfreqdiscriminator();

    rule r1(rg_flag == 0);
        freqdisc.get_inputs(-107237400,40008851);
        rg_flag <= 1;
    endrule

    rule r2(rg_flag == 1);
        let a = freqdisc.results;
        $display($time," Frequency difference = %d \n",a);
        $finish;
    endrule
endmodule : mkTestbench