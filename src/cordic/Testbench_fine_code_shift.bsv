import fine_code_shift::*;
import cordicpipe_main::*;
import magnitudepipe::*;
import BRAMCore::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Integer samp1 = valueOf(`Samples);
    Integer prn = valueOf(`Max_PRN)*1024;
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_val_12",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_12",False);  

    Ifc_magnitudepipe#(`Magnitude_cordic_iterations,`Magnitude_scalling_in_processing,`Magnitude_length) magnitudepipe <- mkmagnitudepipe();
    Ifc_cordicpipe_main#(`Angle_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Cordic_additional_scalling,`Data_processed) cordicpipe <- mkcordicpipe_main(i_val,q_val);
    Ifc_fine_code_shift#(`Samples,`Input_length,`Frequency_component_length,`Max_spcc,`Max_PRN,TMin#(`Common_hspcc,`Different_hspcc),`Odd_spcc,`Even_spcc,`Magnitude_length) fine_codeshift <- mkfine_code_shift(prn_val,prn_val_sample,cordicpipe,magnitudepipe);

    Reg#(Bit#(3)) rg_flag <- mkReg(0);

    rule r1(rg_flag == 0);
        cordicpipe.initialise(1276142157,0,0,1);
        fine_codeshift.get_inputs(954,fromInteger(valueOf(TMul#(TMin#(`Common_hspcc,`Different_hspcc),1))),30);// Inputs are coarse code prn, stop sample and satellite number
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        let a = fine_codeshift.result_corrected_prn_sample;
        $display($time," Fine frequency output : %d \n",a);
        $finish;
    endrule
endmodule