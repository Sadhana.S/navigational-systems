package freqdiscriminator;

    import FixedPoint::*;

    interface Ifc_freqdiscriminator#(numeric type angle_length,
                                     numeric type samples
                                     );
        method Action get_inputs(Int#(angle_length) phase_in1, Int#(angle_length) phase_in2);
        method Int#(angle_length) results;
    endinterface : Ifc_freqdiscriminator

    module mkfreqdiscriminator(Ifc_freqdiscriminator#(angle_length,samples) ifc_freqdisc)
                               provisos(Add#(a,TAdd#(TLog#(samples),1),angle_length));

        Bit#(TAdd#(TLog#(samples),1)) samples = fromInteger(valueOf(samples));
        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);

        Reg#(Int#(angle_length)) rg_phase_diff <- mkReg(0);
        Reg#(Int#(angle_length)) rg_freq_diff <- mkReg(0);

        rule phase_difference_per_sample(rg_readFlag==1);
            Int#(angle_length) temp = unpack(zeroExtend(samples));
            rg_freq_diff <= rg_phase_diff/temp;//remove division with another logic
            rg_readFlag <= 0;
        endrule

        method Action get_inputs(Int#(angle_length) phase_in1, Int#(angle_length) phase_in2) if(rg_readFlag == 0);
            rg_phase_diff <= phase_in1 - phase_in2;
            rg_readFlag <= 1;
        endmethod 
        method Int#(angle_length) results() if(rg_readFlag == 0);
            return rg_freq_diff;
        endmethod

    endmodule : mkfreqdiscriminator
endpackage : freqdiscriminator