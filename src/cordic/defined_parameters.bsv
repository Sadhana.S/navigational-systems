//To add the total number of samples in i_val and q_val

`define Max_PRN 32
`define Max_channel 11
`define Doppler_interval 500
`define Fine_doppler_interval 10
`define Doppler_frequency_shift 10000
`define Data_processed 50 //Number of ms of data processed

/*`define Sampling_frequency 12000000
`define Intermediate_frequency 3563000
`define Samples 12000
`define Max_spcc 12
`define Min_spcc 11
`define Odd_spcc 11
`define Even_spcc 12
`define Common_hspcc 6
`define Different_hspcc 5
`define Start_frequency 3553000
`define Fine_start_frequency 3552750*/

`define Sampling_frequency 16367600
`define Intermediate_frequency 4130400
`define Samples 16367
`define Max_spcc 16
`define Min_spcc 15
`define Odd_spcc 15
`define Even_spcc 16
`define Common_hspcc 8
`define Different_hspcc 7
`define Start_frequency 4120400
`define Fine_start_frequency 4120150

`define Frequency_shift_iterations TAdd#(TDiv#(TMul#(`Doppler_frequency_shift,2),`Doppler_interval),1)
`define Fine_frequency_shift_iterations TAdd#(TDiv#(`Doppler_interval,`Fine_doppler_interval),1)
`define Input_length 4
`define Frequency_component_length 8
`define Angle_length 32
`define Cordic_iterations 16
`define Cordic_additional_scalling 4
`define Samples_accumulated TMax#(`Common_hspcc, `Different_hspcc) 
`define Samples_correlated 2046
`define Magnitude_cordic_iterations 16
`define Magnitude_scalling_in_processing 3
`define Accumulated_length TAdd#(TAdd#(`Input_length,`Frequency_component_length),TLog#(TAdd#(`Max_spcc,1)))
`define Magnitude_length TAdd#(TLog#(`Samples_correlated),`Accumulated_length)
`define Systolic_cells 64
`define Systolic_iterations TDiv#(`Samples_correlated,`Systolic_cells)
`define Cordic_additional_scalling_arctan 4
`define Cordic_iterations_arctan 31
`define Cordic_iterations_division 16