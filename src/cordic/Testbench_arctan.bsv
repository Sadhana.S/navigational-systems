import arctan::*;
import Real::*;
import Vector::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Ifc_arctan#(`Angle_length,`Magnitude_length,`Cordic_additional_scalling_arctan,`Cordic_iterations_arctan) cordic_arctan <- mkarctan;

    Reg#(Bit#(3)) flag <- mkReg(0);
    Reg#(Int#(`Magnitude_length)) i <- mkReg(136667);
    Reg#(Int#(`Magnitude_length)) q <- mkReg(17926);

    rule r1(flag==0);
        cordic_arctan.get_inputs(136667,17926);
        flag <= 1;
    endrule
    rule r2(flag==1);
        Int#(`Angle_length) a = cordic_arctan.result_phase();
        Int#(`Magnitude_length) b = cordic_arctan.result_magnitude();
        cordic_arctan.get_inputs(136667,136667);
        i <= 136667;
        q <= 13667;
        $display("\n For I_P = %d and Q_P = %d, atan(Q_P/I_P) = %d and magnitude = %d",i,q,a,b);
        flag <= 2;
    endrule
    rule r3(flag==2);
        Int#(`Angle_length) a = cordic_arctan.result_phase();
        Int#(`Magnitude_length) b = cordic_arctan.result_magnitude();
        $display("\n For I_P = %d and Q_P = %d, atan(Q_P/I_P) = %d and magnitude = %d",i,q,a,b);
        $finish;
    endrule
endmodule : mkTestbench