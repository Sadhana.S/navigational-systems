/*
1. I_val and Q_val paths in codicpipe_acq.bsv(line 60 & 61) and fine_frequency_shift.bsv(line 57 & 58)
2. prn_val file in codecorrelator.bsv (line 52) and fine_frequency_shift.bsv(line 59)
3. In cordicpipe_acq.bsv, change the variable 'mult_base' as 1023/(No. of samples per ms)(line 47 or 48). Similarly, in fine_frequency_shift.bsv (line 47 or 48)
4. In acquisitionfsm.bsv, in rule r1_inputs_to_freqsynthesiser, change start_freq to Intermediate_frequency - 10000 (line 65 or 66)
5. In acquisitionfsm.bsv, in rule r1_inputs_to_freqsynthesiser, inputs to freqsynthesiser are (doppler_interval,2^32/sampling_frequency,start_frequency,0) (line 67 or 68)
6. In acquisitionfsm.bsv, in r7_frequency_conversion, comment line 136 or 137 according to sampling frequency
7. In acquisitionfsm.bsv, in r7_frequency_conversion, comment line 138 or 139 according to sampling frequency
8. In cordicpipe_acq.bsv and fine_frequency_shift.bsv, change the number of samples (line 17)
*/
import acquisitionfsm::*;
import BRAMCore::*;
import FixedPoint::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Bit#(TLog#(`Max_PRN)) prn_ref = fromInteger(valueOf(`Max_PRN)-1);
    Reg#(Bit#(TLog#(`Max_PRN))) rg_prn <- mkReg(0);
    Integer samp1 = valueOf(`Samples);
    Integer prn = valueOf(`Max_PRN)*1024;

    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
/*
    //For sampling frequency 12MHz
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_val_12",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_12",False); 
    FixedPoint#(TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) samp_freq = 357.913941333; 
*/    
    
    //For sampling frequency 16367600 Hz
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_value_16",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_16",False); 
    FixedPoint#(TAdd#(TSub#(`Angle_length,TLog#(`Sampling_frequency)),2),20) samp_freq = 262.406662919; 
    
    Ifc_acquisitionfsm#(`Doppler_interval,`Fine_doppler_interval,`Start_frequency,`Fine_start_frequency,`Angle_length,`Sampling_frequency,`Accumulated_length,`Samples,`Frequency_shift_iterations,`Fine_frequency_shift_iterations,`Cordic_iterations,`Input_length,`Frequency_component_length,`Common_hspcc,`Different_hspcc,`Cordic_additional_scalling,TAdd#(`Max_spcc,1),`Max_PRN,`Magnitude_cordic_iterations,`Magnitude_scalling_in_processing,`Magnitude_length,`Samples_correlated,`Systolic_cells,`Systolic_iterations,`Odd_spcc,`Even_spcc,`Data_processed) acquisitionfsm <- mkacquisitionfsm(i_val,q_val,prn_val,prn_val_sample,samp_freq);

    Reg#(Bit#(2)) flag <- mkReg(0);

    rule r0(flag == 0);
        acquisitionfsm.initialise;
        $display($time,"Sampling frequency = %d \t Intermediate frequency = %d \n ",`Sampling_frequency,`Intermediate_frequency);
        flag <= 1;
    endrule
    rule r1((rg_prn <= prn_ref)&&(flag == 1));
        acquisitionfsm.prn_value(rg_prn);
        flag <= 2;
    endrule
    rule r2(flag == 2);//values for gven prn_value in rule r1
        let acq_status = acquisitionfsm.acquisition_status;
        let code_shift = acquisitionfsm.code_phase_out;
        let freq_error = acquisitionfsm.frequency_error;
        let fine_freq_error = acquisitionfsm.fine_frequency_error;
        if(acq_status == True)
            $display($time,"Satellite %d acquired with code phase %d, Coarse frequency shift %d and Fine frequency shift %d \n",rg_prn + 1,code_shift,freq_error,fine_freq_error);
        rg_prn <= rg_prn + 1;
        flag <= 1;
        if(rg_prn == prn_ref)
            $finish;
    endrule
endmodule : mkTestbench
