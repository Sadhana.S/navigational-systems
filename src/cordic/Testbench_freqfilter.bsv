import freqfilter::*;
`include "defined_parameters.bsv"

module mkTestbench();

Ifc_freqfilter#(`Angle_length) freqfilter <- mkfreqfilter(3,88);

//Reg#(Int#(32)) rg_kf1 <- mkReg(7);//0.0305*2^8
//Reg#(Int#(32)) rg_kf2 <- mkReg(177);//0.695*2^8

Reg#(Bit#(1)) flag <- mkReg(0);
Reg#(Int#(`Angle_length)) freq1 <- mkReg(29824);
Reg#(Int#(`Angle_length)) freq2 <- mkReg(335);

rule r1(flag==0);
    freqfilter.get_inputs(freq1,freq2);
    flag <= 1;
endrule

rule r2(flag==1);
    let a = freqfilter.results();
    $display("\n Input phases = %d and %d  Output of phase filter = %d",freq1,freq2,a);
    $finish;
endrule

endmodule

