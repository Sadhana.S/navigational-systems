//Second order frequency filter after frequency discriminator in FLL
//Input : Previous two frequencies from frequency discriminator and the previous carrier frequency updated in the last tracking iteration
//Output: Carrier frequency to the nco for the next tracking iteration
package freqfilter;

interface Ifc_freqfilter#(numeric type angle_length);
    method Action get_inputs(Int#(angle_length) freq_in1, Int#(angle_length) freq_in2);
    method Int#(angle_length) results();
endinterface : Ifc_freqfilter

module mkfreqfilter(Int#(16) kf1, Int#(16) kf2, Ifc_freqfilter#(angle_length) ifc_frequencyfilter)
                    provisos(Add#(a,16,angle_length));

    Reg#(Int#(angle_length)) rg_freqout <- mkReg(0);

    method Action get_inputs(Int#(angle_length) freq_in1, Int#(angle_length) freq_in2);
        Int#(angle_length) temp = zeroExtend(kf1)*freq_in1 + zeroExtend(kf2)*freq_in2;
        //$display($time,"Frequency filter inputs : frequency 1 = %d and frequency 2 = %d \n",freq_in1,freq_in2);
        rg_freqout <= temp >> 15;
    endmethod
    method Int#(angle_length) results();
        return rg_freqout;
    endmethod
endmodule : mkfreqfilter

endpackage : freqfilter