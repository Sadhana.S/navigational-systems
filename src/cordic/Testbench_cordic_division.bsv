import cordic_division::*;
`include "defined_parameters.bsv"

module mkTestbench();
    Ifc_cordic_division#(`Magnitude_length,TMin#(`Common_hspcc,`Different_hspcc),`Cordic_iterations_division) cordic <- mkcordic_division();

    Reg#(Bit#(5)) rg_flag <- mkReg(0);

    rule r1(rg_flag == 0);
        cordic.get_inputs(365170,154807);
        //cordic.get_inputs(3,4);
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        let a = cordic.result;
        $display($time,"Output = %d \n", a);
        $finish;
    endrule
endmodule : mkTestbench