import cordicpipe::*;
import freqsynthesiser::*;
import Real::*;
import Vector::*;
import FixedPoint::*;

typedef 10 Samples;
typedef 500 Interval;
typedef 12000000 Sampling_frequency;
typedef 10000 Stop_frequency;

module mkTestbench();

    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer interval1 = valueOf(Interval);
    Int#(16) interval = fromInteger(interval1);
    Integer samp_freq1 = valueOf(Sampling_frequency);
    Int#(32) samp_freq = fromInteger(samp_freq1);
    Integer stop_freq = valueOf(Stop_frequency);
    Int#(16) stop = fromInteger(stop_freq);

    Ifc_cordicpipe ifc_tb1 <- mkcordicpipe;
    Ifc_freqsynthesiser ifc_tb2 <- mkfreqsynthesiser;

    Reg#(Bit#(16)) count1 <- mkReg(0);
    Reg#(Bit#(6)) count3 <- mkReg(0);

    Reg#(Int#(5)) flag <- mkReg(0);
    Reg#(Bit#(2)) readFlag <- mkReg(0);

    Reg#(Int#(33)) base <- mkReg(0);
    Reg#(Bit#(6)) iter <- mkReg(0);

    rule ro(flag == 0);
        Bit#(16) a = fromInteger((2*stop_freq)/interval1);
        FixedPoint#(16,32) start = -fromInteger(stop_freq);
        ifc_tb2.get_inputs(interval,samp_freq,start,0);
        iter <= truncate(a)+1;
        flag <= 1;
    endrule
    rule r1(flag == 1);
        Int#(33) a = ifc_tb2.results_base();
        Int#(33) b = ifc_tb2.results_interval();
        ifc_tb1.initialise(a,b,iter);
        flag <= 2;
    endrule
    rule r2((flag==2)&&(count1<samp));
        ifc_tb1.readbram(count1);
        count1 <= count1+1;
        if(count1==0)
            readFlag <= 1;
    endrule
    rule r3(readFlag == 1);
        let cosine <- ifc_tb1.bramcosine;
        let sine <- ifc_tb1.bramsine;
        $display($time," %d,%d : From testbench : I_out = %d and Q_out = %d \n",count3,count1,cosine,sine);
        if(count1 == samp)
        begin
            readFlag <= 0;
            ifc_tb1.next_iter();
        end
    endrule
    rule r4((count1==samp)&&(count3<=iter-1));
        count3 <= count3 + 1;
        count1 <= 0;
        if(count3 == iter-1)
            $finish;
    endrule
endmodule : mkTestbench