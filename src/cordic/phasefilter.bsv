/*
Module name: Phase Filter
Description: This module computes the second order phase filter after phase discriminator in PLL
			 Input : Previous two phases from discriminator
			 		 the current tracking iteration
			 Output: Phase to the nco for the next tracking iteration
			 The output phase is computed as kp1*phase_in1 + kp2*phase_in2, where
			 	kp1=1;
				phase_in1= Previous phase shift from discriminator
				phase_in2= Current phase shift from NCO in tracking
//TODO is a register required at the output?
*/

package phasefilter;

    interface Ifc_phasefilter#(numeric type angle_length
                              );
        method Action get_inputs(Int#(angle_length) phase_in1,Int#(angle_length) phase_in2);
        method Int#(angle_length) results;
    endinterface : Ifc_phasefilter

    module mkphasefilter(Int#(2) kp1, Int#(2) kp2, Ifc_phasefilter#(angle_length) ifc_phasefilt)
                        provisos(Add#(3,angle_length,b),
                                 Add#(a,2,b)
                                 );

        Reg#(Int#(angle_length)) rg_phase_out <- mkReg(0);

        method Action get_inputs(Int#(angle_length) phase_in1,Int#(angle_length) phase_in2);
            Int#(TAdd#(angle_length,3)) a = (signExtend(kp1)*signExtend(phase_in1))+(signExtend(kp2)*signExtend(phase_in2));
            rg_phase_out <= truncate(a);
        endmethod
        method Int#(angle_length) results;
            return rg_phase_out;
        endmethod

    endmodule : mkphasefilter
endpackage : phasefilter
