import elp_correlator::*;
import cordicpipe_main::*;
import freqsynthesiser::*;
import FixedPoint::*;
import BRAMCore::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Integer prn = 1024*fromInteger(valueOf(`Max_PRN));
    Integer samp = valueOf(`Data_processed)*valueOf(`Samples);

    BRAM_PORT#(Bit#(TAdd#(TLog#(`Max_PRN),10)),Bit#(1)) prn_val <- mkBRAMCore1Load(prn,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_16",False); 
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_value_16",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);

    Ifc_cordicpipe_main#(`Angle_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Cordic_additional_scalling,`Data_processed) cordicpipe <- mkcordicpipe_main(i_val,q_val);
    Ifc_elp_correlator#(`Samples,`Input_length,`Frequency_component_length,`Angle_length,`Magnitude_length,`Data_processed,TAdd#(`Max_spcc,1),`Max_PRN,`Even_spcc,`Odd_spcc) multadd <- mkelp_correlator(fromInteger(valueOf(TMin#(`Common_hspcc,`Different_hspcc))),prn_val,prn_val_sample,cordicpipe);

    Reg#(Bit#(4)) rg_flag <- mkReg(0);

    rule r1(rg_flag == 0);
        cordicpipe.initialise(1082990751,0,1470586728,17);
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        multadd.get_codeshift(921,5,2);
        rg_flag <= 2;
    endrule 
    rule r3(rg_flag == 2);
        let ie = multadd.result_ie;
        let qe = multadd.result_qe;
        let temp_IP = multadd.result_ip;
        let temp_QP = multadd.result_qp;
        let il = multadd.result_il;
        let ql = multadd.result_ql;
        $display($time,"\n IE = %d, QE = %d, IP = %d, QP = %d, IL = %d and QL = %d",ie,qe,temp_IP,temp_QP,il,ql);
        $finish;
    endrule 


endmodule : mkTestbench