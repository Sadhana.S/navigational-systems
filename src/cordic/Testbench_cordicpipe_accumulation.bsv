import cordicpipe_main::*;
import cordicpipe_accumulation::*;
import BRAMCore::*;
`include "defined_parameters.bsv"

module mkTestbench();

    Integer samp1 = valueOf(`Samples);
    Integer prn = valueOf(`Max_PRN)*1024;
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) i_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/I_val_12",False);
    BRAM_PORT#(Bit#(36),Int#(`Input_length)) q_val <- mkBRAMCore1Load(samp1,False,"/home/sadhana/repos/navigational-systems/src/cordic/q_value",False);
    BRAM_PORT#(Bit#(10),Bit#(1)) prn_val_sample <- mkBRAMCore1Load(1023,False,"/home/sadhana/repos/navigational-systems/src/cordic/prn_value_samples_12",False);  

    Reg#(Bit#(3)) rg_flag <- mkReg(0);
    Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
    Reg#(Bit#(TLog#(`Samples))) rg_count <- mkReg(0);
    Reg#(Bit#(TLog#(`Samples_correlated))) rg_bram_count <- mkReg(0);

    Ifc_cordicpipe_main#(`Angle_length,`Samples,`Cordic_iterations,`Input_length,`Frequency_component_length,`Cordic_additional_scalling,`Data_processed) cordicpipe <- mkcordicpipe_main(i_val,q_val);
    Ifc_cordicpipe_accumulation#(`Input_length,`Frequency_component_length,`Samples_correlated,`Accumulated_length,TAdd#(`Max_spcc,1),`Common_hspcc,`Different_hspcc) accumulate <- mkcordicpipe_accumulation(prn_val_sample,cordicpipe);
   
    rule r1(rg_flag == 0);
        cordicpipe.initialise(1271668230,178956,0,0);
        accumulate.initialise;
        rg_flag <= 1;
    endrule
    rule r2(rg_flag == 1);
        rg_flag <= 2;
    endrule
    rule r3(rg_flag == 2);
        accumulate.bram_address(rg_bram_count);
        rg_bram_count <= rg_bram_count + 1;
        if(rg_bram_count == 0)
            rg_readFlag <= 1;
        if(rg_bram_count == fromInteger(valueOf(`Samples_correlated)-1))
            rg_flag <= 3;
    endrule
    rule r4(rg_readFlag == 1);
        let i <- accumulate.accumulated_i_out;
        let q <- accumulate.accumulated_q_out;
        $display($time," %d : i = %d and q = %d",rg_bram_count,i,q);
        if(rg_flag == 3)
            $finish;
    endrule
endmodule