package nco;
	import initial_Settings::*;
	import Vector::*;
	import FixedPoint::*;
	import BRAMCore::*;
	import cordicRot::*;

	interface NcoIfc;
		method Action readInputs(Int#(8) fBin, Int#(16) th, Int#(16) spc);
		method Action reqReadBRAM(Int#(16) k);
		method Int#(16) readSin();
		method Int#(16) readCos();
	endinterface

	module mkNco(NcoIfc);
		Reg#(Int#(8)) frqBin <- mkReg(0);
		Reg#(Int#(16)) theta <- mkReg(0);
		Reg#(Int#(16)) samples_per_code <- mkReg(0);

		BRAM_PORT#(Int#(16),Int#(16)) sine <- mkBRAMCore1(12000,False);
		BRAM_PORT#(Int#(16),Int#(16)) cosine <- mkBRAMCore1(12000,False);

		Reg#(Int#(16)) sin <- mkReg(0);
		Reg#(Int#(16)) cos <- mkReg(0);

		Reg#(FixedPoint#(32,16)) angle <- mkReg(0);
		Reg#(Bit#(2)) quad <- mkReg(0);
		Reg#(Int#(16)) count <- mkReg(0);
		Reg#(FixedPoint#(32,16)) temp <- mkReg(0);

		Reg#(Bit#(4)) flag <- mkReg(-1);
		Reg#(Bit#(0)) doneFlag <- mkReg(0);

		CordicRotIfc ifc <- mkCordic;

		rule computeTime(flag == 0 && count <= 11999);
			FixedPoint#(32,16) lv1 = fromInt(samples_per_code);
			FixedPoint#(32,16) lv2 = fromInt(count+1);
			FixedPoint#(49,16) t1 = fxptQuot(lv2,lv1);
			temp <= fxptTruncate(t1);
			flag <= 1;
		endrule
		rule computeAngle(flag == 1 && count <= 11999);
			angle <= 2*fromInt(frqBin)*3.14159*temp;
			flag <= 2;
		endrule
		rule reduceAngle(flag == 2 && count <= 11999);
			let temp1 = angle;
			let tempQ = 0;
			if(temp1 > 1.57079 && temp1 < 3.14159)
			begin
				$display("Q2");
				temp1 = temp1 - 1.570796;
				tempQ = 1;
			end
			else if(temp1 >= 3.14159 && temp1 < 4.71239)
			begin
				$display("Q3");
				temp1 = temp1 - 3.14159;
				tempQ = 2;
			end

			else if(temp1 >= 4.71239 && temp1 < 6.28318)
			begin
				$display("Q4");
				temp1 = temp1 - 4.71239;
				tempQ = 3;
			end
			else if(temp1 >= 6.28318)
			begin
				temp1 = temp1 - 6.28318;
			end
			if(temp1 >= 0 && temp1 <= 1.57079)
			begin
				$display("Q1");
				angle <= temp1;
				quad <= tempQ;
				flag <= 3;
			end
		endrule
		rule sendData(flag == 3);
			ifc.readIn(fxptTruncate(angle));
			flag <= 4;
		endrule
		rule receiveData(flag == 4);
			FixedPoint#(16,16) lv_sin = fxptSignExtend(ifc.sine());
			FixedPoint#(16,16) lv_cos = fxptSignExtend(ifc.cosine());
			if(quad == 1)
			begin
				let temp = lv_cos;
				lv_cos = -lv_sin;
				lv_sin = temp;
			end
			else if(quad == 2)
			begin
				lv_sin = -lv_sin;
				lv_cos = -lv_cos;
			end
			else if(quad == 3)
			begin
				let temp = lv_sin;
				lv_sin = -lv_cos;
				lv_cos = temp;
			end

			sin <= fxptGetInt(lv_sin * 10000);
			cos <= fxptGetInt(lv_cos * 10000);

			flag <= 5;
		endrule
		rule writeData(flag == 5);
			sine.put(True,count,sin);
			cosine.put(True,count,cos);
			count <= count+1;
			flag <= 0;
		endrule

		method Action readInputs(Int#(8) fBin, Int#(16) th, Int#(16) spc);
			frqBin <= fBin;
			theta <= th;
			samples_per_code <= spc;
			flag <= 0;
		endmethod

		method Action reqReadBRAM(Int#(16) k);
			sine.put(False,k,?);
			cosine.put(False,k,?);
		endmethod

		method Int#(16) readSin();
			Int#(16) x = sine.read();
			return x;
		endmethod

		method Int#(16) readCos();
			Int#(16) x = cosine.read();
			return x;
		endmethod
	endmodule // mkNco

	module mkTb();
		NcoIfc ifc <- mkNco;
		Reg#(Bit#(2)) flag <- mkReg(0);
		
		rule x1(flag == 0);
			ifc.readInputs(1,0,12000);
		endrule


	endmodule // mkTb
endpackage